import {createContext, useContext} from 'react';

export type IProvider = {
  simulateSignin: boolean;
  setSignin: (p: boolean) => void;
};
export const AppContext = createContext<IProvider>({
  simulateSignin: false,
  setSignin: (p: boolean) => null,
});

export const useAppContext = () => useContext(AppContext);
