import STATIC from './constant/index';
const axios = require('axios');

export interface dataWithToken {
  url: string;
  data: any;
  token?: any;
}
/**
 * @param arg string type request
 * @param token optionelle token parametre
 * @returns {object} object header with token
 */
const headers = (arg = 'application/json', token = '') => ({
  'Content-Type': arg,
  Authorization: `Bearer ${token}`,
  Accept: 'application/json',
});

/**
 * @param arg string type request
 * @returns {object} object header without token
 */
const headerWithoutToken = (arg = 'application/json') => ({
  'Content-Type': arg,
  Accept: 'application/json',
});
/***
 *@returns instance of axios
 */
const axiosProvider = axios.create({
  timeout: 120000, // 2 min
  baseURL: STATIC.baseUrl,
});

/**
 * @param data data to send
 * @param argument of data to check if form data or string
 */
export const datas = (data: any, arg = 'application/json') => {
  if (arg === 'application/json') {
    return JSON.stringify(data);
  } else if (arg === 'multipart/form-data') {
    const formData = new FormData();
    Object.keys(data).forEach(key => {
      formData.append(key, data[key]);
    });
    return formData;
  } else {
    return data;
  }
};
/**
 * @param url string url to call api post
 * @param data object to send
 * @param token token to send
 * @returns result of post response
 */
export const postData = async (url: string, data: any, token: any) => {
  const result = await axiosProvider({
    method: 'POST',
    url: url,
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    data: JSON.stringify(data),
  });

  return result;
};
/**
 * @param url string url to call api post
 * @param token token to send
 * @returns result of get response
 */
export const getData = async (url: string, token: any) => {
  try {
    const result = await axiosProvider({
      method: 'get',
      url,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
        Accept: 'application/json',
      },
    });
    return result;
  } catch (e: any) {
    return e.response;
  }
};
/**
 * @param params object to send
 * @returns result of get response
 */
export const putData = async (params: dataWithToken) => {
  try {
    const result = axiosProvider({
      method: 'put',
      url: params.url,
      data: params.url,
      headers: {
        Authorization: params.token,
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    });
    return result;
  } catch (e: any) {
    console.log('[api.js], catch: ', e?.message); // undefined
    return null;
  }
};

export const deleteData = async (params: dataWithToken) => {
  try {
    const result = axiosProvider({
      method: 'delete',
      url: params.url,
      headers: {
        Authorization: params?.token,
        'Content-Type': 'text/plain',
        Accept: 'application/json',
      },
    });
    return result;
  } catch (e: any) {
    console.log('[api.js], catch: ', e?.message); // undefined
    return null;
  }
};

export const putPhoto = async (params: dataWithToken, arg: any) => {
  try {
    const result = await axiosProvider({
      method: 'put',
      url: params.url,
      data: datas(params?.data, arg),
      headers: headers(arg, params.token),
    });
    return result;
  } catch (e: any) {
    console.log('[api.js], catch: ', e?.message); // undefined
    return null;
  }
};

export const postPhoto = async (params: dataWithToken, token: string = '') => {
  const result = await axiosProvider({
    method: 'POST',
    url: params.url,
    data: datas(params.data, 'multipart/form-data'),
    headers: headers('multipart/form-data', token),
  });
  return result;
};

export const postWithoutToken = async (url: string, data: any, arg?: any) => {
  try {
    const result = await axiosProvider({
      method: 'post',
      url: url,
      data: data,
      headers: headerWithoutToken(),
    });
    return result;
  } catch (error: any) {
    console.log('[api.js], catch: ', error.response); // undefined
    return error?.response;
  }
};

export const postWithoutTokenCombineUrl = async (
  url: string,
  param: string,
) => {
  try {
    const result = await axiosProvider({
      method: 'post',
      url: `${url}/${param}`,
      headers: headerWithoutToken(),
    });
    return result;
  } catch (e: any) {
    console.log('[api.js], catch: ', e?.message); // undefined
    return e?.response;
  }
};

export const getDataWithoutToken = async (
  url: string,
  data?: any,
  token?: string,
) => {
  try {
    const result = await axiosProvider({
      method: 'get',
      url,
      headers: headers('application/json', token),
      params: data,
    });
    return result;
  } catch (e: any) {
    console.log('[api.js], catch: ', e);
    return e;
  }
};

//https://www.smashingmagazine.com/2020/05/typescript-modern-react-projects-webpack-babel/
