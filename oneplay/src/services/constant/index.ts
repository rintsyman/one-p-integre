/* eslint-disable import/no-anonymous-default-export */
const axios = require('axios').default;
let env = 'preprod';
/**
 * @constant
 * @type string
 * @default url dev
 */
const DEV = 'https://preprod1.wylog.com/devinsider2-dev/public/';
/**
 * @constant
 * @type string
 * @default url preprod
 */
const PREPROD = 'https://preprod1.wylog.com/devinsider2/public/';
export default {
  baseUrl: env === 'dev' ? DEV : PREPROD,
  //baseUrl: "http://localhost/devinsider2/public/",
};
/**
 * @constant
 * @type string
 * @default url drupal pour recuperation template
 */
export const baseUrlDrupal = 'https://devinsiderbo.wylog.com/';
