import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {RootStackHomeParamList} from '../../../routes/menu-stack/menu-one-stack';
type SigninScreenNavigationProp = NativeStackNavigationProp<
  RootStackHomeParamList,
  'InOutQuiz'
>;
export default function UseINoUT() {
  const navigation = useNavigation<SigninScreenNavigationProp>();

  const goFlash = () => {
    navigation.navigate('Flash');
  };

  const navigateToNotif = () => {
    navigation.navigate('Notification');
  };

  return {goFlash, navigateToNotif};
}
