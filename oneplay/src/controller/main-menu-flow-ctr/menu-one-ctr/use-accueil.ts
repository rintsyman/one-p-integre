import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {RootStackHomeParamList} from '../../../routes/menu-stack/menu-one-stack';
type SigninScreenNavigationProp = NativeStackNavigationProp<
  RootStackHomeParamList,
  'Home'
>;
export default function AccueilCtr() {
  const navigation = useNavigation<SigninScreenNavigationProp>();

  const goEnterMatch = () => {
    navigation.navigate('EnterMatch');
  };

  const navigateToNotif = () => {
    navigation.navigate('Notification');
  };

  return {goEnterMatch, navigateToNotif};
}
