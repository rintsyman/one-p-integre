import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {RootStackHomeParamList} from '../../../routes/menu-stack/menu-one-stack';
type SigninScreenNavigationProp = NativeStackNavigationProp<
  RootStackHomeParamList,
  'Notification'
>;
export default function Notif() {
  const navigation = useNavigation<SigninScreenNavigationProp>();

  const goAllNotif = () => {
    navigation.navigate('AllNotification');
  };

  return {goAllNotif};
}
