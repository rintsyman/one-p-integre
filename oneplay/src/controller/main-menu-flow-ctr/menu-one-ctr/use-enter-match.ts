import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {RootStackHomeParamList} from '../../../routes/menu-stack/menu-one-stack';
type SigninScreenNavigationProp = NativeStackNavigationProp<
  RootStackHomeParamList,
  'EnterMatch'
>;
export default function UseEnterMatch() {
  const navigation = useNavigation<SigninScreenNavigationProp>();

  const goInOut = () => {
    navigation.navigate('InOutQuiz');
  };

  return {goInOut};
}
