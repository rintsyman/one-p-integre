import React from 'react';
import {useAppContext} from '../../provider/index';
import {useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {RootStackParamList} from '../../routes/authentification-stack';
type SignupScreenNavigationProp = NativeStackNavigationProp<
  RootStackParamList,
  'Signup'
>;
export default function UseSignup() {
  const navigation = useNavigation<SignupScreenNavigationProp>();
  const {simulateSignin, setSignin} = useAppContext();

  const completeProfil = () => {
    navigation.navigate('CompleteInfoSignup');
  };

  const goSignin = () => {
    navigation.navigate('Signin');
  };

  return {completeProfil, goSignin};
}
