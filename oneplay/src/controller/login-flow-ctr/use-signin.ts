import React from 'react';
import {useAppContext} from '../../provider/index';
import {useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {RootStackParamList} from '../../routes/authentification-stack';
type SigninScreenNavigationProp = NativeStackNavigationProp<
  RootStackParamList,
  'Signin'
>;
export default function SigninCtr() {
  const navigation = useNavigation<SigninScreenNavigationProp>();
  const {simulateSignin, setSignin} = useAppContext();

  const goForgotPassWord = () => {
    navigation.navigate('ForgotPassword');
  };

  const signin = () => {
    setSignin(true);
  };

  const goSignup = () => {
    navigation.navigate('Signup');
  };

  return {goForgotPassWord, signin, goSignup};
}
