import React, {useEffect} from 'react';

export default function SliderCtr(props: any) {
  const goLogin = () => {
    props?.navigation.navigate('Signin');
  };
  const goSignup = () => {
    props?.navigation.navigate('Signup');
  };

  useEffect(() => {
    async function setTime() {
      setTimeout(() => {
        goLogin();
      }, 2000);
    }
    setTime();
  }, []);

  return {goLogin, goSignup};
}
