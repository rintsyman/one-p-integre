import React from 'react';

export default function UseForgotPassword(props: any) {
  const goSentEmail = () => {
    props?.navigation.navigate('EmailSent');
  };

  return {goSentEmail};
}
