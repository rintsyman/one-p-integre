import {StyleSheet} from 'react-native';
import res from '../index';
export const baseStyle = StyleSheet.create({
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  mediumText: {
    fontSize: 20,
    fontFamily: res.fonts.AvenirMedium,
  },
  smallText: {
    fontSize: 15,
    fontFamily: res.fonts.AvenirMedium,
  },
  baseColor: {
    color: '#f16627',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 25,
    paddingRight: 25,
    height: '100%',
    backgroundColor: 'white',
  },

  smallIcon: {
    height: 20,
    width: 20,
    borderRadius: 20 / 2,
  },
});
