import {Platform} from 'react-native';
export const fonts = {
  AvenirMedium: Platform.OS === 'android' ? 'AvenirLTStd-Medium' : '',
  AvenirBlack: Platform.OS === 'android' ? 'AvenirLTStd-Black' : '',
  AvenirBlackOblique:
    Platform.OS === 'android' ? 'AvenirLTStd-BlackOblique' : '',
  AvenirHeavy: Platform.OS === 'android' ? 'AvenirLTStd-Heavy' : '',
  AvenirLigth: Platform.OS === 'android' ? 'AvenirLTStd-Light' : '',
};
