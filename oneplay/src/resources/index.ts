import {colors} from './colors/index';
import {fonts} from './fonts/fonts';
// import { langues } from "./i18n/index";
import {images} from './images/index';

export default {images, colors, fonts};
