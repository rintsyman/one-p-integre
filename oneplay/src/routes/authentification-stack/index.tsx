import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
//All screen flow-navigation
import SplashScreen from '../../screen/container/splashScreen';
import Signin from '../../screen/container/flow-auth/signin/index';
import Signup from '../../screen/container/flow-auth/signup/index';
import CompleteSignup from '../../screen/container/flow-auth/complete-signup/index';
import ForgotPassword from '../../screen/container/flow-auth/forgot-pwd';
import EmailSent from '../../screen/container/flow-auth/email-sent/index';

export type RootStackParamList = {
  Slider: undefined;
  Signin: undefined;
  Signup: undefined;
  CompleteInfoSignup: undefined;
  ForgotPassword: undefined;
  EmailSent: undefined;
};

const Stack = createNativeStackNavigator<RootStackParamList>();

export default function LoginFlow() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Slider" component={SplashScreen} />
      <Stack.Screen name="Signin" component={Signin} />
      <Stack.Screen name="Signup" component={Signup} />
      <Stack.Screen name="CompleteInfoSignup" component={CompleteSignup} />
      <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
      <Stack.Screen name="EmailSent" component={EmailSent} />
    </Stack.Navigator>
  );
}
