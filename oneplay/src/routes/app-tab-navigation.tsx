import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import LinearGradient from 'react-native-linear-gradient';

import FirstTab from './menu-stack/menu-one-stack';
import SecondTab from './menu-stack/menu-two-stack';
import ThirdTab from '../screen/container/main-menu/menu-tree/index';

import res from '../resources/index';
import {getWindowWidth} from '../screen/components/header';
const Tab = createBottomTabNavigator();

export default function AppNavigation() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerTitle: '',
        headerShown: false,
        tabBarShowLabel: false,
        tabBarBackground: () => (
          <View
            style={{
              borderTopColor: res.colors.ColorGreen,
              borderTopWidth: StyleSheet.hairlineWidth,
            }}>
            <LinearGradient
              style={{
                width: getWindowWidth(),
                height: 65,
                flexDirection: 'row',
                justifyContent: 'space-around',
                borderTopColor: res.colors.ColorGreen,
                borderTopWidth: StyleSheet.hairlineWidth,
                alignItems: 'center',
                alignContent: 'center',
              }}
              colors={['rgba(3, 12, 86, .95)', 'rgba(3, 5, 43, 1)']}
              start={{x: 0, y: 0}}
              end={{x: 0, y: 1}}
              locations={[0, 1]}
            />
          </View>
        ),
      }}>
      <Tab.Screen
        name="FirstTab"
        component={FirstTab}
        options={{
          tabBarIcon: ({color, size, focused}) => (
            <Image
              source={focused ? res.images.MENU1ON : res.images.MENU1}
              style={stylesMenu.menuImage}
              width={30}
              height={30}
            />
          ),
        }}
      />

      <Tab.Screen
        name="SecoundTab"
        component={SecondTab}
        options={{
          tabBarIcon: ({color, size, focused}) => (
            <Image
              source={focused ? res.images.MENU3ON : res.images.MENU3}
              style={[stylesMenu.menuImage]}
            />
          ),
        }}
      />

      <Tab.Screen
        name="FifthTab"
        component={ThirdTab}
        options={{
          tabBarIcon: ({color, size, focused}) => (
            <Image
              source={focused ? res.images.MENU5ON : res.images.MENU5}
              style={stylesMenu.menuImage}
              width={30}
              height={30}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const stylesMenu = StyleSheet.create({
  menuImage: {
    width: 38,
    height: 38,
    resizeMode: 'contain',
  },
});
