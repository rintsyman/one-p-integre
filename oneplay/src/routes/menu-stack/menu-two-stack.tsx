import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
//All screen flow-navigation
import LaunchCamera from '../../screen/container/main-menu/menu-two/index';
import Camera from '../../screen/container/main-menu/menu-two/camera';

export type RootStackCameraParamList = {
  LaunchCamera: undefined;
  Camera: undefined;
};

const Stack = createNativeStackNavigator<RootStackCameraParamList>();

export default function LoginFlow() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Camera" component={Camera} />
      <Stack.Screen name="LaunchCamera" component={LaunchCamera} />
    </Stack.Navigator>
  );
}
