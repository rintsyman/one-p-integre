import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
//All screen flow-navigation
import Home from '../../screen/container/main-menu/menu-one/accueil/index';
import EnterMatch from '../../screen/container/main-menu/menu-one/enter-match/index';
import InOutQuiz from '../../screen/container/main-menu/menu-one/in-out-quiz';
import Flash from '../../screen/container/main-menu/menu-one/flash/index';
import Notification from '../../screen/container/main-menu/notification/index';
import {AllNotification} from '../../screen/container/main-menu/notification/view-all-notif/index';

export type RootStackHomeParamList = {
  Home: undefined;
  EnterMatch: undefined;
  InOutQuiz: undefined;
  Flash: undefined;
  Notification: undefined;
  AllNotification: undefined;
};

const Stack = createNativeStackNavigator<RootStackHomeParamList>();

export default function LoginFlow() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="EnterMatch" component={EnterMatch} />
      <Stack.Screen name="InOutQuiz" component={InOutQuiz} />
      <Stack.Screen name="Flash" component={Flash} />
      <Stack.Screen name="Notification" component={Notification} />
      <Stack.Screen name="AllNotification" component={AllNotification} />
    </Stack.Navigator>
  );
}
