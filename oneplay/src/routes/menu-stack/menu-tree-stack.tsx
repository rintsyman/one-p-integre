import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
//All screen flow-navigation
import Tchat from '../../screen/container/main-menu/menu-tree/index';

export type RootStackTchatParamList = {
  Tchat: undefined;
};

const Stack = createNativeStackNavigator<RootStackTchatParamList>();

export default function LoginFlow() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Tchat" component={Tchat} />
    </Stack.Navigator>
  );
}
