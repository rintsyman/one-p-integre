import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
//All screen flow-navigation
import AppTab from './app-tab-navigation';
import Camera from '../screen/container/main-menu/menu-two/camera';

const Stack = createNativeStackNavigator();

export default function LoginFlow() {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="AppTab">
      <Stack.Screen name="AppTab" component={AppTab} />
      <Stack.Screen name="Camera" component={Camera} />
    </Stack.Navigator>
  );
}
