import React from 'react';
import {StyleProp, View, ViewStyle} from 'react-native';

export type Iprops = {
  customStyle?: StyleProp<ViewStyle>;
};
export default function DividerRow(props: Iprops) {
  return (
    <View
      style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View
        style={[
          {width: '100%', backgroundColor: '#8E8E8E', height: 0.5},
          props.customStyle,
        ]}
      />
    </View>
  );
}
