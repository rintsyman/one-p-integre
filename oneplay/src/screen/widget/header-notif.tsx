/*************Import from react lib********************/
import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  ImageBackground,
  Dimensions,
  StyleSheet,
  Alert,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';

import {connect} from 'react-redux';

import {ifIphoneX} from 'react-native-iphone-x-helper';

/*************Import from source code********************/

import res from '../../resources/index';
import {Header, Badge, Icon} from 'react-native-elements';
import {getWindowWidth} from '../../utils/utils';
import {styles} from '../components/header/styles';

export const HeaderNotif = () => {
  /*************Render PARTS********************/

  const _renderIconLeft = () => {
    return (
      <TouchableOpacity
        style={[HeaderStyle.headerAlignementCenter, {width: 45, height: 45}]}
        onPress={() => null}>
        <Image
          source={res.images.notif}
          style={{
            width: 30,
            height: 230,
            resizeMode: 'contain',
            alignSelf: 'center',
          }}
        />
        <Badge
          status={'error'}
          badgeStyle={{borderWidth: 0}}
          containerStyle={{
            position: 'absolute',
            top: 0,
            right: 0,
            padding: 0,
            borderWidth: 0,
          }}
          value={8}
        />
      </TouchableOpacity>
    );
  };

  const _renderIconRight = () => {
    return (
      <TouchableOpacity
        style={[HeaderStyle.headerAlignementCenter, {width: 45, height: 45}]}
        // onPress= {() => this.props.navigate }
        onPress={() => null}>
        <Image
          source={res.images.burger}
          style={{
            width: 32,
            height: 32,
            resizeMode: 'contain',
            alignSelf: 'center',
          }}
        />
      </TouchableOpacity>
    );
  };

  const _renderHeaderCenter = () => {
    return (
      <View
        style={[
          HeaderStyle.headerAlignementCenter,
          {width: getWindowWidth() - 130},
        ]}>
        <Text style={HeaderStyle.textTitleWhite}>Notifications</Text>
      </View>
    );
  };
  /*************Render methode********************/

  return (
    <Header
      containerStyle={[
        HeaderStyle.headerAlignementCenter,
        {
          backgroundColor: res.colors.ColorBlackO0,
          flexDirection: 'row',

          borderBottomWidth: 0,
          ...ifIphoneX(
            {
              height: 100,
              paddingTop: 30,
            },
            {
              height: 80,
              paddingTop: 0,
            },
          ),
        },
      ]}
      backgroundImage={res.images.back_ground_menu}
      barStyle="default"
      placement="center"
      leftComponent={_renderIconLeft()}
      centerComponent={_renderHeaderCenter()}
      rightComponent={_renderIconRight()}
    />
  );
};

const HeaderStyle = StyleSheet.create({
  headerAlignementCenter: {
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    position: 'relative',
    zIndex: 9999,
  },
  fleshBack: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
    // backgroundColor:'red'
  },
  logoLeft: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
  },
  searchClick: {
    borderRadius: 30,
    backgroundColor: '#03052C',
    alignContent: 'center',
    justifyContent: 'flex-start',
    paddingRight: 100,
    paddingBottom: 5,
    paddingTop: 5,
    paddingLeft: 5,
  },
  searchClickimage: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
  },
  textTitleWhite: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    color: res.colors.ColorWhite,
    lineHeight: 20,
  },
});
