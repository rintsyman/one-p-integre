import React, {Children, useState} from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Image,
  StyleProp,
  ViewStyle,
  ImageStyle,
  TextStyle,
} from 'react-native';
import res from '../../resources/index';
export type IProps = {
  icone: any;
  placeholder: string;
  isSecure?: boolean;
  customBorderStyle?: StyleProp<ViewStyle>;
  customIconeStyle?: StyleProp<ImageStyle>;
  customTextInputStyle?: StyleProp<TextStyle>;
  children?: React.ReactChild;
  isNotEdit?: boolean;
  placeHolderTextColor?: string;
};

export default function InputText(props: IProps) {
  const [isFocused, setIsFocused] = useState(false);

  const handleFocus = () => setIsFocused(true);

  const handleBlur = () => setIsFocused(false);
  return (
    <View
      style={[
        styles?.formTextInput,
        {
          borderColor: isFocused
            ? res.colors.ColorGreen
            : res.colors.ColorGreyO5,
        },
        props.customBorderStyle,
      ]}>
      <Image
        source={props?.icone}
        resizeMode={'contain'}
        style={[{width: 20, height: 20}, props?.customIconeStyle]}
      />
      {props?.children}
      <TextInput
        style={[
          {
            width: '90%',
            fontFamily: res.fonts.AvenirMedium,
            height: 45,
          },
          props.customTextInputStyle,
        ]}
        placeholderTextColor={
          props?.placeHolderTextColor ? props?.placeHolderTextColor : 'gray'
        }
        placeholder={props?.placeholder}
        secureTextEntry={props?.isSecure || false}
        onFocus={handleFocus}
        onBlur={handleBlur}
        editable={props?.isNotEdit ? false : true}
      />
    </View>
  );
}

export const styles = StyleSheet.create({
  formTextInput: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderWidth: 1,
    paddingHorizontal: 5,
    borderRadius: 5,
    marginVertical: 5,
    paddingVertical: -5,
  },
});
