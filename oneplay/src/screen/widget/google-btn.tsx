import React from 'react';
import {Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import res from '../../resources/index';

export default function GoogleBtn() {
  return (
    <TouchableOpacity style={styles?.googleBtn}>
      <Image source={res.images.google} style={{width: 30, height: 30}} />
      <Text style={styles?.googleTxt}> Continuer avec google</Text>
    </TouchableOpacity>
  );
}

export const styles = StyleSheet.create({
  googleBtn: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderWidth: 2,
    borderRadius: 5,
    borderColor: 'gray',
    width: '100%',
  },
  googleTxt: {
    fontFamily: 'SourceSansPro-SemiBold',
    fontSize: 20,
    paddingVertical: 10,
  },
});
