import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
export const HeaderTitleCenter = () => {
  return (
    <View style={styles?.header}>
      <Text style={styles?.titleDigit}>Digimarket</Text>
    </View>
  );
};

export const styles = StyleSheet.create({
  header: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  titleDigit: {
    color: '#845FB4',
    fontSize: 45,
    fontFamily: 'SourceSansPro-SemiBold',
  },
});
