import React, {Children} from 'react';
import {StyleProp, View, ViewStyle} from 'react-native';

export type Iprops = {
  children: React.ReactChild;
  containerStyle?: StyleProp<ViewStyle>;
  alignement: 'row' | 'column';
};
export const Center = (props: Iprops) => {
  return (
    <View
      style={[
        {
          flexDirection: props?.alignement,
          justifyContent: 'center',
          alignItems: 'center',
          display: 'flex',
          flex: 1,
        },
        props?.containerStyle,
      ]}>
      {props?.children}
    </View>
  );
};
