import React from 'react';
import {View, Text} from 'react-native';
import {getWindowWidth} from '../../utils/utils';
import res from '../../resources/index';
export default function Divider() {
  return (
    <View
      style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: getWindowWidth() / 20,
        marginRight: getWindowWidth() / 20,
      }}>
      <View style={{width: '45%', backgroundColor: '#8E8E8E', height: 0.5}} />
      <Text style={{paddingHorizontal: 5, fontFamily: res.fonts.AvenirMedium}}>
        Or
      </Text>
      <View style={{width: '45%', backgroundColor: '#8E8E8E', height: 0.5}} />
    </View>
  );
}
