import React from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  StyleProp,
  ViewStyle,
  TextStyle,
} from 'react-native';
import res from '../../resources/index';

export type IProps = {
  label: string;
  customBoutonStyle?: StyleProp<ViewStyle>;
  onPress?: () => void;
  customLabelStyle?: StyleProp<TextStyle>;
};
export const BoutonWidget = (props: IProps) => {
  return (
    <TouchableOpacity
      style={[styles?.bouton, props?.customBoutonStyle]}
      onPress={props?.onPress}>
      <Text style={[styles?.txtBouton, props?.customLabelStyle]}>
        {props.label}
      </Text>
    </TouchableOpacity>
  );
};

export const styles = StyleSheet.create({
  bouton: {
    justifyContent: 'center',
    backgroundColor: res.colors.ColorGreen,
    fontFamily: res.fonts.AvenirBlack,
    alignContent: 'center',
    alignItems: 'center',
    height: '40%',
    borderRadius: 10,
    color: 'white',
  },
  txtBouton: {
    color: 'white',
    fontSize: 20,
    fontFamily: res.fonts.AvenirMedium,
  },
});
