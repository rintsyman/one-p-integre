import React from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  View,
  Image,
  StyleProp,
  ViewStyle,
} from 'react-native';
import {baseStyle} from '../../resources/style/base';
export type IProps = {
  icone_left: any;
  icon_rigth: any;
  label_center?: string;
  customContainerHeader?: StyleProp<ViewStyle>;
  onLeftBtnPress?: () => void;
  onRigthBtnPress?: () => void;
};

export default function Header(props: IProps) {
  return (
    <View style={[styles?.containerHeader, props.customContainerHeader]}>
      <TouchableOpacity
        onPress={props?.onLeftBtnPress}
        style={{
          height: 50,
          width: 50,
          position: 'relative',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          source={props?.icone_left}
          style={[styles?.avatar, baseStyle?.smallIcon]}
          resizeMode={'contain'}
        />
      </TouchableOpacity>
      <Text style={baseStyle?.mediumText}>{props?.label_center}</Text>
      <TouchableOpacity
        onPress={props?.onRigthBtnPress}
        style={{
          height: 50,
          width: 50,
          position: 'relative',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          source={props?.icon_rigth}
          style={[styles?.rigth_icon, baseStyle?.smallIcon]}
          resizeMode={'contain'}
        />
      </TouchableOpacity>
    </View>
  );
}

export const styles = StyleSheet.create({
  containerHeader: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: Dimensions.get('screen').height / 12,
  },

  avatar: {
    width: 35,
    height: 35,
    borderRadius: 25 / 2,
  },

  rigth_icon: {
    width: 35,
    height: 35,
    borderRadius: 25 / 2,
  },
  arrowIcon: {
    position: 'absolute',
    right: 15,
  },
});
