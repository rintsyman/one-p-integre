import React from 'react';
import {View, Text, ImageBackground, StyleSheet} from 'react-native';
import res from '../../resources/index';

export type Iprops = {
  children?: React.ReactChild;
};
export default function BackGroungTheme(props: Iprops) {
  return (
    <View style={{flex: 1}}>
      <ImageBackground
        style={styles.WrapperBg}
        source={res.images.back_groug_general}>
        <ImageBackground style={styles.WrapperBg} source={res.images.filtre}>
          {props.children}
        </ImageBackground>
      </ImageBackground>
    </View>
  );
}

export const styles = StyleSheet.create({
  textCenter: {
    textAlign: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  WrapperBg: {
    flex: 1,
    position: 'relative',
  },
  Title: {
    fontFamily: 'Avenir-Medium',
    fontSize: 17,
    color: res.colors.ColorWhite,
  },

  textGreyStyle14O5: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    color: res.colors.ColorWhiteO25,
  },
  listStyle: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  textTitleWhite: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    color: res.colors.ColorWhite,
    lineHeight: 20,
  },
});
