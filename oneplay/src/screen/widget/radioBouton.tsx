import React from 'react';
import BouncyCheckbox from 'react-native-bouncy-checkbox';
import {
  View,
  Text,
  StyleSheet,
  StyleProp,
  ViewStyle,
  TextStyle,
} from 'react-native';
import {baseStyle} from '../../resources/style/base';
import res from '../../resources/index';

export type Iprops = {
  label: string;
  containerStyle?: StyleProp<ViewStyle>;
  radioStyle?: StyleProp<ViewStyle>;
  labelStyle?: StyleProp<TextStyle>;
  onChecked: (isChecked: boolean | undefined) => void;
};
export default function RadioBouton(props: Iprops) {
  return (
    <View style={[styles?.container, props?.containerStyle]}>
      <BouncyCheckbox
        size={20}
        iconStyle={{borderColor: res.colors.ColorGreen}}
        fillColor={res.colors.ColorGreen}
        textStyle={{fontFamily: res.fonts.AvenirMedium}}
        onPress={(isChecked: boolean | undefined) =>
          props?.onChecked(isChecked)
        }
        style={props?.radioStyle}
      />
      <Text
        style={[
          baseStyle?.smallText,
          styles?.txtSmallStyle,
          props?.labelStyle,
        ]}>
        {props?.label}
      </Text>
    </View>
  );
}

export const styles = StyleSheet.create({
  txtSmallStyle: {
    fontFamily: res.fonts.AvenirMedium,
    alignSelf: 'flex-start',
    paddingVertical: 15,
    flexShrink: 1,
  },

  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
    left: 35,
  },
});
