import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  ActivityIndicator,
  ImageBackground,
  Image,
} from 'react-native';
import res from '../../resources/index';
import {useNavigation} from '@react-navigation/native';
import useSliderScreen from '../../controller/login-flow-ctr/use-splash-screen';
export default function SplashScreen() {
  const navigation = useNavigation();
  const useSlider = useSliderScreen({navigation});

  return (
    <ImageBackground
      source={res.images.back_ground}
      style={{
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
      }}>
      <ImageBackground
        source={res.images.filtre}
        style={{
          flex: 1,
          flexDirection: 'column',
          width: '100%',
          height: '100%',
          justifyContent: 'flex-end',
          alignItems: 'center',
        }}>
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}>
          <Image
            style={{
              flex: 10,
              resizeMode: 'contain',
              alignSelf: 'flex-end',
              width: 300,
            }}
            source={res.images.logo_txt}
          />
          <ActivityIndicator style={{flex: 1}} size="large" color="#45D8B7" />
        </View>
      </ImageBackground>
    </ImageBackground>
  );
}
