import React from 'react';
import {
  View,
  Text,
  TouchableWithoutFeedback,
  Keyboard,
  TouchableOpacity,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {styles} from './styles';
import res from '../../../../resources/index';
import {baseStyle} from '../../../../resources/style/base';
import {BoutonWidget} from '../../../widget/bouton-cusom';
import HeaderTitleCenter from '../../../components/header/index';
import Divider from '../../../widget/divider';
import DividerRow from '../../../widget/divider-row';
import WidgetTextInput from '../../../widget/input-text';
import WidgetGooGleBtn from '../../../widget/google-btn';
import useSignup from '../../../../controller/login-flow-ctr/use-signup';
export default function Signup() {
  const providerSignup = {...useSignup()};
  return (
    <KeyboardAwareScrollView
      keyboardShouldPersistTaps="always"
      alwaysBounceVertical={false}
      showsVerticalScrollIndicator={false}
      scrollEnabled={true}
      extraScrollHeight={250}
      contentContainerStyle={{flexGrow: 1}}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={true}>
        <View style={{flex: 1}}>
          <HeaderTitleCenter />

          <View style={baseStyle.container}>
            <View style={[styles.containerGoogle, baseStyle?.center]}>
              <Text style={[baseStyle?.mediumText, styles?.txtStyle]}>
                Creer votre compte gratuit
              </Text>
              <WidgetGooGleBtn />
            </View>
            <View style={styles.containerLigne}>
              <Divider />
            </View>
            <View style={styles.containerForm}>
              <WidgetTextInput
                icone={res?.images?.mail}
                placeholder={'Adresse email'}
              />
              <View style={styles.containerBtn}>
                <BoutonWidget
                  label={'Continuer avec Email'}
                  customBoutonStyle={{
                    height: 45,
                    backgroundColor: res.colors.ColorGreen,
                  }}
                  onPress={() => providerSignup.completeProfil()}
                />
              </View>
              <View style={styles.containerAlreadyAccount}>
                <DividerRow />
                <TouchableOpacity onPress={() => providerSignup.goSignin()}>
                  <Text
                    style={[baseStyle?.mediumText, styles?.txtStyleAlready]}>
                    Avoir un compte? Identifier Vous
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAwareScrollView>
  );
}
