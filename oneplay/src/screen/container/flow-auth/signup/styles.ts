import {StyleSheet} from 'react-native';
import res from '../../../../resources/index';
export const styles = StyleSheet.create({
  containerGoogle: {
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'space-evenly',
  },
  containerLigne: {
    flex: 0.5,
  },
  containerForm: {
    flex: 3,
  },
  txtStyle: {
    paddingBottom: 25,
    fontSize: 20,
  },

  containerBtn: {
    flex: 0.8,
    justifyContent: 'center',
  },
  containerAlreadyAccount: {
    flex: 1,
  },
  txtStyleAlready: {
    fontSize: 18,
    textAlign: 'center',
    paddingVertical: 15,
    color: res.colors.ColorGreen,
  },
});
