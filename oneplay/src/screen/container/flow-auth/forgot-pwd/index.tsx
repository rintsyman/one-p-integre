import React, {useState} from 'react';
import {View, Text, Image} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {styles} from './style';
import res from '../../../../resources/index';
import {baseStyle} from '../../../../resources/style/base';
import {BoutonWidget} from '../../../widget/bouton-cusom';
import WidgetTextInput from '../../../widget/input-text';
import {Center} from '../../../widget/center';
import UseForgotPassword from '../../../../controller/login-flow-ctr/use-forgot-password';
import {useNavigation} from '@react-navigation/native';
export default function ForgotPassword() {
  const navigation = useNavigation();
  const useForgotPassword = UseForgotPassword({navigation});
  return (
    <KeyboardAwareScrollView
      keyboardShouldPersistTaps="handled"
      alwaysBounceVertical={false}
      showsVerticalScrollIndicator={false}
      scrollEnabled={true}
      extraHeight={130}
      extraScrollHeight={130}
      contentContainerStyle={{flexGrow: 1, paddingHorizontal: 25}}>
      <View style={styles?.container_icon}>
        <Image
          source={res.images.update_pwd}
          height={45}
          width={45}
          style={styles?.iconSend}
        />
        <Text style={baseStyle?.mediumText}>Modifier votre mot de passe</Text>
        <Center containerStyle={{paddingVertical: 25}} alignement="column">
          <>
            <Text style={[baseStyle?.smallText, styles?.customSmallText]}>
              Entrer votre adresse email et click sur envoyer email pour
              continuer
            </Text>
            <WidgetTextInput
              placeholder="Adresse Email"
              icone={res.images.mail}
              customBorderStyle={{marginVertical: 25}}
            />
          </>
        </Center>
        <Center
          alignement="row"
          containerStyle={{
            justifyContent: 'space-between',
            width: '100%',
            paddingVertical: 20,
          }}>
          <>
            <BoutonWidget
              label="Annuler"
              customBoutonStyle={[styles?.reset_email, styles?.cancelCustom]}
              customLabelStyle={{color: '#845FB4'}}
            />
            <BoutonWidget
              label="Envoyer email"
              customBoutonStyle={[styles?.reset_email, styles?.customSendMail]}
              onPress={useForgotPassword?.goSentEmail}
            />
          </>
        </Center>
      </View>
    </KeyboardAwareScrollView>
  );
}
