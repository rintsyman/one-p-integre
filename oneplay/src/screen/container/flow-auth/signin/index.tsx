import React from 'react';
import {
  View,
  Text,
  KeyboardAvoidingView,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Image,
} from 'react-native';
import HeaderFondnoBack from '../../../components/header/index';
import {styles} from './style';
import res from '../../../../resources';
import Divider from '../../../widget/divider';
import InputForm from '../../../widget/input-text';
import {getWindowWidth} from '../../../../utils/utils';
import UseSignin from '../../../../controller/login-flow-ctr/use-signin';

export default function Index() {
  const {goForgotPassWord, goSignup, signin} = UseSignin();
  return (
    <View style={{flex: 1}}>
      <HeaderFondnoBack />
      <View style={{flex: 1}}>
        <View style={[styles.viewCntainer, styles.containerView]}>
          <View style={styles.viewContainerScrollView}>
            <View style={[styles.viewContainerForm]}>
              <InputForm
                icone={res.images.mail}
                placeholder="Email"
                customIconeStyle={{
                  resizeMode: 'contain',
                }}
                customBorderStyle={{marginVertical: 25}}
              />

              <InputForm
                icone={res.images.mail}
                placeholder="Password"
                customIconeStyle={{
                  resizeMode: 'contain',
                }}
              />
              <TouchableOpacity
                style={styles.viewContainerTxtMdpOublier}
                onPress={goForgotPassWord}>
                <Text
                  style={{
                    color: res.colors.ColorGreen,
                    fontFamily: res.fonts.AvenirMedium,
                  }}>
                  Mot de passe oublier
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={[styles.viewContaitBtn, {paddingTop: 60, marginTop: 10}]}>
              <View style={[styles.viewContentTouchable2Connect]}>
                <TouchableOpacity
                  style={[
                    styles.touchableBtnInscriptionConnect,
                    {backgroundColor: res.colors.ColorGreen},
                  ]}
                  onPress={() => signin()}>
                  <Text
                    style={[
                      styles.txtInscription,
                      {color: res.colors.ColorWhite},
                    ]}>
                    Connecté
                  </Text>
                  <Image
                    source={res.images.flesh_black}
                    style={styles.imageInTouchableConnect}
                  />
                </TouchableOpacity>
              </View>

              <Divider />

              <View
                style={[
                  styles.viewContentTouchable2Connect,
                  {
                    marginBottom: '25%',
                  },
                ]}>
                <TouchableOpacity
                  style={styles.touchableBtnInscriptionConnect}
                  onPress={goSignup}>
                  <Text style={styles.txtInscription}>Inscrire</Text>
                  <Image
                    source={res.images.flesh_green}
                    style={styles.imageInTouchableConnect}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}
