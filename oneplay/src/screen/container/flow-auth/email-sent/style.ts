import {StyleSheet} from 'react-native';
import res from '../../../../resources/index';

export const styles = StyleSheet.create({
  iconSend: {
    width: 200,
    height: 200,
  },
  container_icon: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  customSmallText: {
    fontFamily: res.fonts.AvenirMedium,
    paddingVertical: 25,
  },
  reset_email: {
    height: 45,
    paddingHorizontal: 25,
  },

  customSendMail: {},
});
