import React, {useState} from 'react';
import {View, Text, Image} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {styles} from './style';
import res from '../../../../resources/index';
import {baseStyle} from '../../../../resources/style/base';
import {BoutonWidget} from '../../../widget/bouton-cusom';
import {Center} from '../../../widget/center';
import {useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {RootStackParamList} from '../../../../routes/authentification-stack';

type EamilSentScreenNavigationProp = NativeStackNavigationProp<
  RootStackParamList,
  'EmailSent'
>;
export default function EmailSent() {
  const navigation = useNavigation<EamilSentScreenNavigationProp>();

  return (
    <KeyboardAwareScrollView
      keyboardShouldPersistTaps="handled"
      alwaysBounceVertical={false}
      showsVerticalScrollIndicator={false}
      scrollEnabled={true}
      extraHeight={130}
      extraScrollHeight={130}
      contentContainerStyle={{
        flexGrow: 1,
        paddingHorizontal: 25,
        paddingVertical: 25,
      }}>
      <View style={styles?.container_icon}>
        <View>
          <Image
            source={res.images.email_send}
            height={45}
            width={45}
            style={styles?.iconSend}
          />
          <Text style={[baseStyle?.mediumText, {alignSelf: 'center'}]}>
            Email envoyer !
          </Text>
        </View>

        <Center alignement="column" containerStyle={{flex: 0}}>
          <>
            <Text style={[baseStyle?.smallText, styles?.customSmallText]}>
              Nous avons envoyer un email de 4 chiffre sur votre adresse email.
              Pour pouvoir connecter click sur connecter et modifier votre mot
              de passe
            </Text>
            <BoutonWidget
              onPress={() => navigation.navigate('Signin')}
              label="Se connecter"
              customBoutonStyle={[styles?.reset_email, styles?.customSendMail]}
            />
          </>
        </Center>
      </View>
    </KeyboardAwareScrollView>
  );
}
