import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import HeaderTitleCenter from '../../../components/header/index';
import {styles} from './styles';
import res from '../../../../resources/index';
import {baseStyle} from '../../../../resources/style/base';
import {BoutonWidget} from '../../../widget/bouton-cusom';
import WidgetTextInput from '../../../widget/input-text';
import RadioButton from '../../../widget/radioBouton';
import ModalPays from './modal.pays';

export default function Signin() {
  const [modalVisible, setModalVisible] = useState(false);

  return (
    <KeyboardAwareScrollView
      keyboardShouldPersistTaps="handled"
      alwaysBounceVertical={false}
      showsVerticalScrollIndicator={false}
      scrollEnabled={true}
      extraHeight={130}
      extraScrollHeight={130}
      contentContainerStyle={{flexGrow: 1}}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={true}>
        <View>
          <HeaderTitleCenter />
          <View style={baseStyle.container}>
            <View style={{paddingVertical: 25}}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={[
                    baseStyle?.mediumText,
                    {fontSize: 30, fontFamily: res.fonts.AvenirMedium},
                  ]}>
                  Complete votre profile
                </Text>
                <Text
                  style={[
                    baseStyle?.smallText,
                    {fontFamily: res.fonts.AvenirLigth},
                  ]}>
                  Complete votre profile
                </Text>
              </View>
              <View style={{flex: 2, paddingVertical: 15}}>
                <WidgetTextInput
                  icone={res?.images?.user}
                  placeholder={'Nom'}
                />
                <WidgetTextInput
                  icone={res?.images?.user}
                  placeholder={'Prenom'}
                />
                <WidgetTextInput
                  icone={res?.images?.cadenas}
                  placeholder={'Mot de passe'}
                  isSecure={true}
                />
                <TouchableOpacity
                  onPress={() => setModalVisible(true)}
                  activeOpacity={1}>
                  <WidgetTextInput
                    icone={null}
                    placeholder={'Country'}
                    isNotEdit={true}>
                    <Image
                      source={res.images.flesh_bas}
                      resizeMode="contain"
                      style={[{width: 20, height: 20}, styles?.arrowIcon]}
                    />
                  </WidgetTextInput>
                </TouchableOpacity>
              </View>
              <View style={{flex: 1}}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    width: '100%',
                  }}>
                  <RadioButton
                    label="Oui ! j'accepte les terme et les agreement policy"
                    onChecked={(p: boolean | undefined) => {}}
                    containerStyle={{
                      left: 0,
                    }}
                  />
                </View>
                <BoutonWidget
                  label="Continuer"
                  customBoutonStyle={{height: 45}}
                  onPress={() => null}
                />
              </View>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
      <ModalPays
        modalVisible={modalVisible}
        closeModal={() => setModalVisible(false)}
      />
    </KeyboardAwareScrollView>
  );
}
