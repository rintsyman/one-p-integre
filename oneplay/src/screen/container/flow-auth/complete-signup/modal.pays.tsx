import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Modal,
  FlatList,
} from 'react-native';

import {styles} from './styles';
import res from '../../../../resources/index';
import {baseStyle} from '../../../../resources/style/base';
import WidgetTextInput from '../../../widget/input-text';
import {mockRegion} from '../../../../utils/mock-regions';
import RadioButton from '../../../widget/radioBouton';

type Iprops = {
  modalVisible: boolean;
  closeModal: () => void;
};
export default function ModalPays(props: Iprops) {
  return (
    <Modal
      animationType="slide"
      transparent={false}
      visible={props?.modalVisible}
      statusBarTranslucent
      onRequestClose={() => props?.closeModal()}>
      <View style={{flex: 1}}>
        <View style={styles.modalHeader}>
          <TouchableOpacity
            onPress={() => props?.closeModal()}
            style={styles?.clauseModal}>
            <Image
              source={res?.images?.close}
              height={25}
              width={25}
              style={{resizeMode: 'contain', width: 25}}
            />
          </TouchableOpacity>

          <View style={styles?.containerPays}>
            <Text style={[baseStyle?.smallText, styles?.findPays]}>
              Selectionner votre pays
            </Text>
          </View>
        </View>
        <View style={[styles.modalHeader, {paddingBottom: 10}]}>
          <View style={styles?.containerPays}>
            <WidgetTextInput
              icone={res?.images?.find}
              placeholder={''}
              isSecure={true}
              customBorderStyle={{
                borderTopWidth: 0,
                borderLeftWidth: 0,
                borderRightWidth: 0,
              }}
              customIconeStyle={{left: 20}}
            />
          </View>
        </View>
        <FlatList
          data={mockRegion}
          keyExtractor={item => item?.id?.toString()}
          initialNumToRender={9}
          maxToRenderPerBatch={9}
          renderItem={element => (
            <RadioButton
              label={element.item?.country_name}
              onChecked={(p: boolean | undefined) => {}}
            />
          )}
          windowSize={9}
        />
      </View>
    </Modal>
  );
}
