import {Dimensions, StyleSheet} from 'react-native';
import res from '../../../../resources/index';
export const styles = StyleSheet.create({
  containerGoogle: {
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'space-evenly',
  },

  modalHeader: {
    display: 'flex',
    flexDirection: 'row',
    paddingTop: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  clauseModal: {
    position: 'absolute',
    left: 35,
    top: 5,
  },
  containerPays: {
    alignSelf: 'center',
    justifyContent: 'center',
  },
  findPays: {
    textAlign: 'center',
    fontSize: 18,
  },
  txtSmallStyle: {
    fontFamily: res.fonts.AvenirMedium,
    alignSelf: 'flex-start',
    paddingVertical: 15,
    flexShrink: 1,
  },
  arrowIcon: {
    position: 'absolute',
    right: 15,
  },
});
