import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  FlatList,
} from 'react-native';
import res from '../../../../../resources/index';
import {styles} from './styles';
import {HeaderLogoCenter} from '../../../../components/header-main/index';
import BackGroungTheme from '../../../../widget/back-groung-theme';
import ListeMatch from './item-liste/index';
import MenuDraw from '../../menu-draw/index';
import HeaderListe from './header-liste/index';
import UseAccueil from '../../../../../controller/main-menu-flow-ctr/menu-one-ctr/use-accueil';
export default function Index() {
  const providerAccueil = {...UseAccueil()};
  let data = Array.from(Array(20).keys());

  return (
    <BackGroungTheme>
      <View style={{flex: 1}}>
        <HeaderLogoCenter showNotif= {() => providerAccueil.navigateToNotif()}/>
        <View
          style={{
            flex: 1,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-around',
          }}>
          <View style={{flex: 1}}>
            <FlatList
              data={data}
              ListHeaderComponent={() => <HeaderListe />}
              keyExtractor={item => item.toString()}
              renderItem={({item}) => (
                <ListeMatch
                  goEnterMatch={() => providerAccueil.goEnterMatch()}
                />
              )}
            />
          </View>
        </View>
      </View>
    </BackGroungTheme>
  );
}
