import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  FlatList,
} from 'react-native';
import {styles} from '../styles';
import res from '../../../../../../resources/index';
import {ModalMatch} from '../items-modal/modal-home';

export default function Index() {
  const [isChoiceClicked, setIsChoiceClicked] = useState<boolean>(false);
  const [isModalShow, setisModalShow] = useState<boolean>(false);
  const [isChampionnat, setIsChampionnat] = useState<boolean>(false);

  const clickFilter = () => {
    setIsChoiceClicked(true);
  };

  return (
    <View>
      <View style={{margin: 20}}>
        <Text style={styles.Title}>Les matchs du jour</Text>
      </View>

      <TouchableOpacity
        style={{
          height: 25,
          display: isChoiceClicked == false ? 'flex' : 'none',
        }}
        onPress={() => {
          clickFilter();
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingLeft: 15,
            paddingRight: 15,
          }}>
          <View>
            <Text
              style={{
                color: res.colors.ColorGreen,
                fontSize: 15,
              }}>
              Championnats/Compétitions
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'flex-start',
            }}>
            <Image
              style={{
                height: 10,
                width: 10,
                resizeMode: 'contain',
              }}
              source={res.images.flesh_bas}
            />
          </View>
        </View>
      </TouchableOpacity>

      <View
        style={{
          display: isChoiceClicked == false ? 'none' : 'flex',
          height: 80,
          flexDirection: 'column',
          justifyContent: 'flex-start',
        }}>
        <TouchableOpacity
          style={{
            marginVertical: 0,
            height: 25,
            paddingLeft: 15,
            paddingRight: 15,
          }}
          onPress={() => {
            setIsChampionnat(true);
            setisModalShow(true);
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                marginBottom: 5,
              }}>
              <Text
                style={{
                  color: res.colors.ColorGreen,
                  fontSize: 15,
                }}>
                Championnats
              </Text>
            </View>

            <Image
              style={{
                height: 10,
                width: 10,
                resizeMode: 'contain',
              }}
              source={res.images.flesh_bas}
            />
          </View>
        </TouchableOpacity>

        <View
          style={{
            marginHorizontal: 10,
            marginVertical: 3,
            borderBottomWidth: StyleSheet.hairlineWidth,
            borderBottomColor: 'rgba(122, 122, 122, .20)',
          }}
        />

        <TouchableOpacity
          style={{
            marginVertical: 0,
            height: 25,
            paddingLeft: 15,
            paddingRight: 15,
          }}
          onPress={() => {
            setIsChampionnat(false);
            setisModalShow(true);
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                marginBottom: 5,
              }}>
              <Text
                style={{
                  color: res.colors.ColorGreen,
                  fontSize: 15,
                }}>
                Compétitions
              </Text>
            </View>

            <Image
              style={{
                height: 10,
                width: 10,
                resizeMode: 'contain',
              }}
              source={res.images.flesh_bas}
            />
          </View>
        </TouchableOpacity>
      </View>
      <ModalMatch
        isModalShow={isModalShow}
        closeModal={() => setisModalShow(false)}
        isChampionnat={isChampionnat}
      />
    </View>
  );
}
