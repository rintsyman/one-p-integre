import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  Image,
  StyleSheet,
  FlatList,
} from 'react-native';
import res from '../../../../../../resources/index';
import {styles} from './styles';

type IProps = {
  goEnterMatch: () => void;
};
export default function ListeMatch(props: IProps) {
  return (
    <View style={styles.homeBox}>
      <View
        style={[
          styles.textLive,
          {
            display: 'flex',
            backgroundColor: 'red',
          },
        ]}>
        <Text style={styles.textLiveIn}>LIVE</Text>
      </View>
      <View style={[styles.homeBlock, {paddingBottom: 0}]}>
        <View style={styles.block}>
          <Image
            style={styles.imageOneStyle}
            source={{
              uri: 'https://apiv3.apifootball.com/badges/logo_leagues/300_copa-del-rey.png',
            }}
          />
          <Image
            source={res.images.cheched}
            style={[styles.checkBoxImageHome, {opacity: 1}]}
          />

          <View style={styles.Teamame}>
            <Text
              style={[
                styles.textGreyStyle12,
                {
                  marginTop: 15,
                  alignItems: 'center',
                },
              ]}>
              barca
            </Text>
            <Text style={[styles.textWhiteStyle12, {alignSelf: 'center'}]}>
              0
            </Text>
          </View>
        </View>

        <View style={styles.viewCenter}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Image source={res.images.vues} style={styles.nbrVues} />
            <Text style={{color: 'white', fontFamily: res.fonts.AvenirMedium}}>
              +250
            </Text>
          </View>
          <View>
            <Text style={[styles.textGreyStyle10, {marginTop: 5}]}>
              10/15/2021
            </Text>
          </View>
          <View>
            <Text style={[styles.textGreyStyle10, styles.hours]}>00:25</Text>
          </View>
        </View>
        <View style={styles.block2}>
          <Image
            style={styles.imageOneStyle}
            source={{
              uri: 'https://apiv3.apifootball.com/badges/logo_leagues/383_super-cup.png',
            }}
          />

          <Image
            source={res.images.cheched}
            style={[styles.checkBoxImageHome, {opacity: 1}]}
          />
          <View style={styles.Teamame}>
            <Text
              style={[
                styles.textGreyStyle12,
                {
                  marginTop: 15,
                  alignItems: 'center',
                },
              ]}>
              juventis
            </Text>
            <Text style={[styles.textWhiteStyle12, {alignSelf: 'center'}]}>
              3
            </Text>
          </View>
        </View>
      </View>

      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-around',
          alignItems: 'center',
          alignContent: 'center',
          marginTop: 5,
          marginBottom: 15,
          width: '100%',
        }}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            padding: 10,
          }}>
          <TouchableOpacity onPress={() => null}>
            <Image
              source={res.images.notif}
              style={{width: 25, height: 25}}
              resizeMode={'contain'}
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={props.goEnterMatch}
          style={[
            styles.btnGreenLittle2,
            {
              backgroundColor: res.colors.ColorGreen,
            },
          ]}
          disabled={false}>
          <Text style={styles.btnGreenLittleTxt}>Entrer dans le Match</Text>
        </TouchableOpacity>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            padding: 10,
          }}>
          <TouchableOpacity onPress={() => null}>
            <Image
              source={res.images.addFriend}
              style={{width: 25, height: 25}}
              resizeMode={'contain'}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}
