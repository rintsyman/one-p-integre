/*************Import from react lib********************/
import React, {Component} from 'react';
import {
  Text,
  View,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';

/*************Import from source code********************/
import res from '../../../../../../resources/index';
import Modal from 'react-native-modal';
import LinearGradient from 'react-native-linear-gradient';

export default function ItemCompetition() {
  return (
    <LinearGradient
      style={{
        position: 'relative',
        paddingTop: 50,
        paddingLeft: 15,
        paddingRight: 15,
        height: '100%',
      }}
      colors={['rgba(3, 5, 43, .0)', 'rgba(3, 5, 43, .5)', 'rgba(3, 5, 43, 1)']}
      start={{x: 0, y: 0}}
      end={{x: 0, y: 1}}
      locations={[0.1, 0.5, 1]}>
      <ScrollView
        style={{
          marginBottom: 15,
          paddingBottom: 25,
        }}
        nestedScrollEnabled={true}>
        <View
          style={{
            marginBottom: 5,
          }}>
          <Text
            style={{
              color: res.colors.ColorGreen,
              fontSize: 25,
            }}>
            Compétitions
          </Text>
        </View>
        <TouchableOpacity onPress={() => {}} style={styles.listStyle}>
          <View
            style={{
              display: 'flex',
            }}>
            <Icon name="trophy" size={15} color={res.colors.ColorGreen} />
          </View>
          <Text
            style={[
              styles.textTitleWhite,
              {fontSize: 14, marginVertical: 15, paddingHorizontal: 15},
            ]}>
            Champions League
          </Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => {}} style={styles.listStyle}>
          <View
            style={{
              display: 'flex',
            }}>
            <Icon name="trophy" size={15} color={res.colors.ColorGreen} />
          </View>
          <Text style={[styles.textTitleWhite, styles.positionItem]}>
            Europa League
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {}} style={styles.listStyle}>
          <View
            style={{
              display: 'flex',
            }}>
            <Icon name="trophy" size={15} color={res.colors.ColorGreen} />
          </View>
          <Text style={[styles.textTitleWhite, styles.positionItem]}>
            Coupe de France
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {}} style={styles.listStyle}>
          <View
            style={{
              display: 'flex',
            }}>
            <Icon name="trophy" size={15} color={res.colors.ColorGreen} />
          </View>
          <Text style={[styles.textTitleWhite, styles.positionItem]}>
            Euro 2020
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => {}} style={styles.listStyle}>
          <View
            style={{
              display: 'flex',
            }}>
            <Icon name="trophy" size={15} color={res.colors.ColorGreen} />
          </View>
          <Text style={[styles.textTitleWhite, styles.positionItem]}>
            Coupe d'Afrique
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {}}
          style={[styles.listStyle, {marginBottom: 30}]}>
          <View
            style={{
              display: 'flex',
            }}>
            <Icon name="trophy" size={15} color={res.colors.ColorGreen} />
          </View>
          <Text style={[styles.textTitleWhite, styles.positionItem]}>
            Coupe du monde
          </Text>
        </TouchableOpacity>
      </ScrollView>
      <LinearGradient
        style={{
          height: 29,
          position: 'absolute',
          bottom: 0,
          left: 0,
          right: 0,
        }}
        colors={[
          'rgba(3, 5, 43, .0)',
          'rgba(3, 5, 43, .5)',
          'rgba(3, 5, 43, 1)',
        ]}
        start={{x: 0, y: 0}}
        end={{x: 0, y: 1}}
        locations={[0.1, 0.3, 1]}
      />
    </LinearGradient>
  );
}

export const styles = StyleSheet.create({
  WrapperBg: {
    position: 'relative',
    width: '100%',
    height: '100%',
  },
  modalPop: {
    margin: 0,
    alignItems: 'center',
  },
  textCenter: {
    textAlign: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },

  Title: {
    fontFamily: res.fonts.AvenirMedium,
    fontSize: 17,
    color: res.colors.ColorWhite,
  },

  textGreyStyle14O5: {
    fontFamily: res.fonts.AvenirMedium,
    fontSize: 14,
    color: res.colors.ColorWhiteO25,
  },
  listStyle: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingVertical: 15,
  },
  textTitleWhite: {
    fontFamily: res.fonts.AvenirMedium,
    fontSize: 18,
    color: res.colors.ColorWhite,
    lineHeight: 20,
  },
  positionItem: {
    fontSize: 14,
    marginVertical: 2,
    paddingHorizontal: 15,
  },
});
