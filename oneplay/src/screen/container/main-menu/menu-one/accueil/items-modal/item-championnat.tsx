/*************Import from react lib********************/
import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';

/*************Import from source code********************/
import res from '../../../../../../resources/index';
import LinearGradient from 'react-native-linear-gradient';

export default function ItemChampionnat() {
  return (
    <LinearGradient
      style={{
        position: 'relative',
        paddingLeft: 15,
        paddingRight: 15,
      }}
      colors={['rgba(3, 5, 43, .0)', 'rgba(3, 5, 43, .5)', 'rgba(3, 5, 43, 1)']}
      start={{x: 0, y: 0}}
      end={{x: 0, y: 1}}
      locations={[0.1, 0.5, 1]}>
      <LinearGradient
        style={{
          position: 'relative',
          paddingTop: 50,
          paddingLeft: 15,
          paddingRight: 15,
        }}
        colors={[
          'rgba(3, 5, 43, .0)',
          'rgba(3, 5, 43, .5)',
          'rgba(3, 5, 43, 1)',
        ]}
        start={{x: 0, y: 0}}
        end={{x: 0, y: 1}}
        locations={[0.1, 0.5, 1]}>
        <ScrollView
          nestedScrollEnabled={true}
          showsVerticalScrollIndicator={false}>
          <View
            style={{
              marginBottom: 5,
            }}>
            <Text
              style={{
                color: res.colors.ColorGreen,
                fontSize: 25,
              }}>
              Championnats
            </Text>
          </View>
          <View style={{flex: 1, flexDirection: 'column'}}>
            <TouchableOpacity>
              <Text
                style={[
                  styles.textGreyStyle14O5,
                  {
                    fontSize: 14,
                    marginVertical: 15,
                  },
                ]}>
                Français
              </Text>
            </TouchableOpacity>
            <View
              style={{
                marginBottom: 5,
              }}>
              <TouchableOpacity
                onPress={() => {}}
                style={{paddingVertical: 15}}>
                <View style={styles.listStyle}>
                  <View
                    style={{
                      display: 'flex',
                    }}>
                    <Icon
                      name="trophy"
                      size={15}
                      color={res.colors.ColorGreen}
                    />
                  </View>
                  <Text
                    style={[
                      styles.textTitleWhite,
                      {
                        fontSize: 13,
                        marginLeft: 20,
                        lineHeight: 20,
                      },
                    ]}>
                    Ligue 1
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => {}}>
                <View style={styles.listStyle}>
                  <View
                    style={{
                      display: 'flex',
                    }}>
                    <Icon
                      name="trophy"
                      size={15}
                      color={res.colors.ColorGreen}
                    />
                  </View>
                  <Text
                    style={[
                      styles.textTitleWhite,
                      {
                        fontSize: 13,
                        marginLeft: 20,
                        lineHeight: 20,
                      },
                    ]}>
                    Ligue 2
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{flex: 1, flexDirection: 'column'}}>
            <TouchableOpacity>
              <Text
                style={[
                  styles.textGreyStyle14O5,
                  {fontSize: 14, marginVertical: 15},
                ]}>
                Anglais
              </Text>
            </TouchableOpacity>
            <View
              style={{
                marginBottom: 5,
              }}>
              <TouchableOpacity onPress={() => {}}>
                <View style={styles.listStyle}>
                  <View
                    style={{
                      display: 'flex',
                    }}>
                    <Icon
                      name="trophy"
                      size={15}
                      color={res.colors.ColorGreen}
                    />
                  </View>
                  <Text
                    style={[
                      styles.textTitleWhite,
                      {
                        fontSize: 13,
                        marginLeft: 20,
                        lineHeight: 20,
                      },
                    ]}>
                    Premier League
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {}}
                style={{paddingVertical: 15}}>
                <View style={styles.listStyle}>
                  <View
                    style={{
                      display: 'flex',
                    }}>
                    <Icon
                      name="trophy"
                      size={15}
                      color={res.colors.ColorGreen}
                    />
                  </View>
                  <Text
                    style={[
                      styles.textTitleWhite,
                      {
                        fontSize: 13,
                        marginLeft: 20,
                        lineHeight: 20,
                      },
                    ]}>
                    Championship
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => {}}>
                <View style={styles.listStyle}>
                  <View
                    style={{
                      display: 'flex',
                    }}>
                    <Icon
                      name="trophy"
                      size={15}
                      color={res.colors.ColorGreen}
                    />
                  </View>
                  <Text
                    style={[
                      styles.textTitleWhite,
                      {
                        fontSize: 13,
                        marginLeft: 20,
                        lineHeight: 20,
                      },
                    ]}>
                    League One
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{flex: 1, flexDirection: 'column'}}>
            <TouchableOpacity>
              <Text
                style={[
                  styles.textGreyStyle14O5,
                  {fontSize: 14, marginVertical: 15},
                ]}>
                Espagnol
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                marginBottom: 5,
              }}
              onPress={() => {}}>
              <View style={styles.listStyle}>
                <View
                  style={{
                    display: 'flex',
                  }}>
                  <Icon name="trophy" size={15} color={res.colors.ColorGreen} />
                </View>
                <Text
                  style={[
                    styles.textTitleWhite,
                    {
                      fontSize: 13,
                      marginLeft: 20,
                      lineHeight: 20,
                    },
                  ]}>
                  Liga
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{flex: 1, flexDirection: 'column'}}>
            <TouchableOpacity onPress={() => null}>
              <Text
                style={[
                  styles.textGreyStyle14O5,
                  {fontSize: 14, marginVertical: 15},
                ]}>
                Italien
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                marginBottom: 5,
              }}
              onPress={() => {}}>
              <View style={styles.listStyle}>
                <View
                  style={{
                    display: 'flex',
                  }}>
                  <Icon name="trophy" size={15} color={res.colors.ColorGreen} />
                </View>
                <Text
                  style={[
                    styles.textTitleWhite,
                    {
                      fontSize: 13,
                      marginLeft: 20,
                      lineHeight: 20,
                    },
                  ]}>
                  Serie A
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{flex: 1, flexDirection: 'column'}}>
            <TouchableOpacity onPress={() => null}>
              <Text
                style={[
                  styles.textGreyStyle14O5,
                  {fontSize: 14, marginVertical: 15},
                ]}>
                Allemand
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                marginBottom: 5,
              }}
              onPress={() => {}}>
              <View style={styles.listStyle}>
                <View
                  style={{
                    display: 'flex',
                  }}>
                  <Icon name="trophy" size={15} color={res.colors.ColorGreen} />
                </View>
                <Text
                  style={[
                    styles.textTitleWhite,
                    {
                      fontSize: 13,
                      marginLeft: 20,
                      lineHeight: 20,
                    },
                  ]}>
                  Bundesliga
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{flex: 1, flexDirection: 'column'}}>
            <TouchableOpacity onPress={() => null}>
              <Text
                style={[
                  styles.textGreyStyle14O5,
                  {fontSize: 14, marginVertical: 15},
                ]}>
                Portugais
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                marginBottom: 5,
              }}
              onPress={() => {}}>
              <View style={styles.listStyle}>
                <View
                  style={{
                    display: 'flex',
                  }}>
                  <Icon name="trophy" size={15} color={res.colors.ColorGreen} />
                </View>
                <Text
                  style={[
                    styles.textTitleWhite,
                    {
                      fontSize: 13,
                      marginLeft: 20,
                      lineHeight: 20,
                    },
                  ]}>
                  Primeira Liga
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{flex: 1, flexDirection: 'column'}}>
            <TouchableOpacity onPress={() => null}>
              <Text
                style={[
                  styles.textGreyStyle14O5,
                  {fontSize: 14, marginVertical: 15},
                ]}>
                Belge
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                marginBottom: 5,
              }}
              onPress={() => {}}>
              <View style={styles.listStyle}>
                <View
                  style={{
                    display: 'flex',
                  }}>
                  <Icon name="trophy" size={15} color={res.colors.ColorGreen} />
                </View>
                <Text
                  style={[
                    styles.textTitleWhite,
                    {
                      fontSize: 13,
                      marginLeft: 20,
                      lineHeight: 20,
                    },
                  ]}>
                  Jupiler League
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              marginBottom: 30,
            }}>
            <TouchableOpacity onPress={() => null}>
              <Text
                style={[
                  styles.textGreyStyle14O5,
                  {fontSize: 14, marginVertical: 15},
                ]}>
                Suisse
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                marginBottom: 5,
              }}
              onPress={() => {}}>
              <View style={styles.listStyle}>
                <View
                  style={{
                    display: 'flex',
                  }}>
                  <Icon name="trophy" size={15} color={res.colors.ColorGreen} />
                </View>
                <Text
                  style={[
                    styles.textTitleWhite,
                    {
                      fontSize: 13,
                      marginLeft: 20,
                      lineHeight: 20,
                    },
                  ]}>
                  Super League
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <LinearGradient
          style={{
            height: 29,
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
          }}
          colors={[
            'rgba(3, 5, 43, .0)',
            'rgba(3, 5, 43, .5)',
            'rgba(3, 5, 43, 1)',
          ]}
          start={{x: 0, y: 0}}
          end={{x: 0, y: 1}}
          locations={[0.1, 0.3, 1]}
        />
      </LinearGradient>

      <LinearGradient
        style={{
          height: 29,
          position: 'absolute',
          bottom: 0,
          left: 0,
          right: 0,
        }}
        colors={[
          'rgba(3, 5, 43, .0)',
          'rgba(3, 5, 43, .5)',
          'rgba(3, 5, 43, 1)',
        ]}
        start={{x: 0, y: 0}}
        end={{x: 0, y: 1}}
        locations={[0.1, 0.3, 1]}
      />
    </LinearGradient>
  );
}

export const styles = StyleSheet.create({
  WrapperBg: {
    position: 'relative',
    width: '100%',
    height: '100%',
  },
  modalPop: {
    margin: 0,
    alignItems: 'center',
  },
  textCenter: {
    textAlign: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },

  Title: {
    fontFamily: res.fonts.AvenirMedium,
    fontSize: 17,
    color: res.colors.ColorWhite,
  },

  textGreyStyle14O5: {
    fontFamily: res.fonts.AvenirMedium,
    fontSize: 14,
    color: res.colors.ColorWhiteO25,
  },
  listStyle: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  textTitleWhite: {
    fontFamily: res.fonts.AvenirMedium,
    fontSize: 18,
    color: res.colors.ColorWhite,
    lineHeight: 20,
  },
});
