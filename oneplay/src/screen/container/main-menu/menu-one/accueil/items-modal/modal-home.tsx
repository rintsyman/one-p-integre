/*************Import from react lib********************/
import React, {Component} from 'react';
import {ImageBackground, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';

/*************Import from source code********************/
import res from '../../../../../../resources/index';
import Modal from 'react-native-modal';
import ItemChampionnat from './item-championnat';
import ItemCompetition from './item-competition';
export type IProps = {
  isModalShow: boolean;
  closeModal: () => void;
  isChampionnat: boolean;
};
export const ModalMatch = (props: IProps) => {
  /*************Render PARTS********************/
  return (
    <Modal
      isVisible={props.isModalShow}
      onBackButtonPress={() => props.closeModal()}
      coverScreen={true}
      backdropColor={'black'}
      style={styles.modalPop}
      animationInTiming={500}
      animationOutTiming={500}
      animationIn={'slideInUp'}
      animationOut={'slideOutDown'}
      statusBarTranslucent>
      <ImageBackground style={styles.WrapperBg} source={res.images.filtre}>
        {props.isChampionnat ? <ItemChampionnat /> : <ItemCompetition />}
      </ImageBackground>
    </Modal>
  );
};

export const styles = StyleSheet.create({
  WrapperBg: {
    position: 'relative',
    width: '100%',
    height: '100%',
  },
  modalPop: {
    margin: 0,
    alignItems: 'center',
  },
});
