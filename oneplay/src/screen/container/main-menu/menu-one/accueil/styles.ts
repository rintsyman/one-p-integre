/*************Import from react lib********************/
import {StyleSheet, Dimensions} from 'react-native';

/*************Import from source code********************/
import res from '../../../../../resources/index';
export const styles = StyleSheet.create({
  textCenter: {
    textAlign: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  WrapperBg: {
    flex: 1,
    position: 'relative',
  },
  Title: {
    fontFamily: res.fonts.AvenirMedium,
    fontSize: 17,
    color: res.colors.ColorWhite,
  },

  textGreyStyle14O5: {
    fontFamily: res.fonts.AvenirMedium,
    fontSize: 14,
    color: res.colors.ColorWhiteO25,
  },
  listStyle: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  textTitleWhite: {
    fontFamily: res.fonts.AvenirMedium,
    fontSize: 18,
    color: res.colors.ColorWhite,
    lineHeight: 20,
  },
});
