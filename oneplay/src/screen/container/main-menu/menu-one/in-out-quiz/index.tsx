import React from 'react';
import {
  Text,
  View,
  ImageBackground,
  ActivityIndicator,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
} from 'react-native';
import {HeaderLogoCenter} from '../../../../components/header-main/index';
import BackGroungTheme from '../../../../widget/back-groung-theme';
import res from '../../../../../resources/index';
import {styles} from './styles';
import UseInOut from '../../../../../controller/main-menu-flow-ctr/menu-one-ctr/use-in-out';
export default function Index() {
  const providerInOut = {...UseInOut()};
  return (
    <BackGroungTheme>
      <View style={{flex: 1}}>
        <HeaderLogoCenter showNotif={() => providerInOut.navigateToNotif()} />
        <ScrollView style={{flex: 1}}>
          <View style={{flex: 1}}>
            <View style={[styles.wpContent, {paddingRight: 10}]}>
              <View style={styles.mainContent}>
                <View
                  style={{
                    marginLeft: -15,
                    marginRight: -15,
                    paddingRight: 15,
                    paddingLeft: 15,
                    position: 'relative',
                  }}>
                  <View style={[styles.gridMatch, {overflow: 'visible'}]}>
                    <View
                      style={[
                        styles.textLive,
                        {
                          backgroundColor: res.colors.ColorGrey,
                        },
                      ]}>
                      <Text style={styles.textLiveIn}>LIVE</Text>
                    </View>
                    <View
                      style={[
                        styles.gridMatchContent,
                        {position: 'relative', overflow: 'visible'},
                      ]}>
                      <View style={styles.equipe}>
                        <View style={styles.logoEquipe1}>
                          <View
                            style={[
                              styles.choixEquipe,
                              {backgroundColor: res.colors.ColorGreen},
                            ]}
                          />
                          <Image
                            style={styles.drapeauEquipe}
                            source={{
                              uri: 'https://apiv3.apifootball.com/badges/logo_leagues/383_super-cup.png',
                            }}
                          />
                        </View>
                        <View
                          style={[
                            styles.score,
                            {
                              justifyContent: 'flex-end',
                              paddingLeft: 15,
                              marginTop: 25,
                              marginBottom: 10,
                            },
                          ]}>
                          <TouchableOpacity onPress={() => null}>
                            <Text style={styles.yellowCard}>5</Text>
                          </TouchableOpacity>
                          <TouchableOpacity onPress={() => null}>
                            <Text style={styles.redCard}>5</Text>
                          </TouchableOpacity>
                          <Text style={styles.scoreNumber}>2</Text>
                        </View>
                      </View>
                      <View style={styles.matchShowdown}>
                        <Text
                          style={{fontSize: 14, color: res.colors.ColorGreen}}>
                          25:15
                        </Text>
                        <View
                          style={[
                            styles.versus,
                            {
                              alignContent: 'center',
                              justifyContent: 'center',
                              alignItems: 'center',
                            },
                          ]}>
                          <Text
                            style={{
                              fontSize: 12,
                              color: res.colors.ColorWhite,
                            }}>
                            VS
                          </Text>
                        </View>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            alignContent: 'center',
                          }}>
                          <Image
                            source={res.images.vues}
                            style={{
                              height: 25,
                              width: 25,
                              resizeMode: 'contain',
                              marginRight: 10,
                            }}
                          />
                          <Text
                            style={[styles.textWhiteStyle12, {color: 'white'}]}>
                            +200
                          </Text>
                        </View>
                      </View>
                      <View style={styles.equipe}>
                        <View style={styles.logoEquipe2}>
                          <Image
                            style={styles.drapeauEquipe}
                            source={{
                              uri: 'https://apiv3.apifootball.com/badges/logo_leagues/300_copa-del-rey.png',
                            }}
                          />
                          <View
                            style={[
                              styles.choixEquipe,
                              {backgroundColor: res.colors.ColorGrey},
                            ]}
                          />
                        </View>
                        <View
                          style={[
                            styles.score,
                            {
                              justifyContent: 'flex-start',
                              paddingLeft: 15,
                              marginTop: 25,
                              marginBottom: 10,
                            },
                          ]}>
                          <Text style={styles.scoreNumber}>0</Text>
                          <TouchableOpacity onPress={() => null}>
                            <Text style={styles.yellowCard}>3</Text>
                          </TouchableOpacity>
                          <TouchableOpacity onPress={() => null}>
                            <Text style={styles.redCard}>25</Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>

                <TouchableOpacity
                  style={styles.gridInOut}
                  onPress={() => providerInOut.goFlash()}
                  //disabled={this.props.in}
                >
                  <ImageBackground
                    source={res.images.in}
                    style={{
                      width: '102%',
                      height: '100%',
                      alignItems: 'center',
                      justifyContent: 'center',
                      position: 'relative',
                    }}>
                    <Text
                      style={
                        // this.props.in
                        //   ? [
                        //       styles.textMatch,
                        //       {opacity: 0.3, borderColor: res.colors.ColorGrey},
                        //     ]
                        //   :
                        styles.textMatch
                      }>
                      IN
                    </Text>
                    <View
                      style={{
                        position: 'absolute',
                        bottom: 20,
                        right: 20,
                        display: 'flex',
                      }}>
                      <TouchableOpacity
                        style={{
                          width: 45,
                          height: 45,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                        onPress={() => null}>
                        <Image
                          source={res.images.mapInactiv}
                          style={{width: 30, height: 30, resizeMode: 'contain'}}
                        />
                      </TouchableOpacity>
                    </View>
                  </ImageBackground>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.gridInOut}
                  onPress={() => providerInOut.goFlash()}>
                  <ImageBackground
                    source={res.images.out}
                    style={{
                      width: '102%',
                      height: '100%',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text style={styles.textMatch}>OUT</Text>
                  </ImageBackground>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </BackGroungTheme>
  );
}
