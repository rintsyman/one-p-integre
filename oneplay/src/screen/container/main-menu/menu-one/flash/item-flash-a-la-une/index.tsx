import React from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';

import res from '../../../../../../resources/index';
import {styles, flashStyle} from '../styles';
import {
  getWindowHeight,
  getWindowWidth,
} from '../../../../../components/header';
import FastImage from 'react-native-fast-image';
import LinearGradient from 'react-native-linear-gradient';

export default function Index() {
  return (
    <View style={{flex: 1, flexDirection: 'column', minHeight: 230}}>
      <TouchableOpacity
        style={[flashStyle.aLaUneGrid, {marginHorizontal: 0}]}
        onPress={() => null}>
        <View
          style={{
            position: 'relative',
            minHeight: 175,
            width: '100%',
          }}>
          <FastImage
            source={{
              uri: 'https://scanlibs.com/wp-content/uploads/android-development-retrofit.jpg',
            }}
            style={{
              flexDirection: 'row',
              width: '100%',
              minHeight: 175,
            }}
            onLoadEnd={() => null}>
            <LinearGradient
              style={[flashStyle.aLaUneFiltre]}
              colors={['transparent', 'transparent', '#000']}>
              <View
                style={{
                  width: getWindowWidth() - 90,
                }}>
                <Text
                  style={[
                    styles.textGreenSmall,
                    {
                      fontWeight: 'bold',
                    },
                  ]}>
                  12:15
                </Text>
                <Text
                  style={[
                    styles.textTitleWhite,
                    {
                      fontWeight: 'bold',
                    },
                  ]}>
                  Jose
                </Text>
                <Text
                  style={[
                    styles.textWiteSmall,
                    {
                      fontWeight: 'bold',
                    },
                  ]}>
                  Deep Learning for Chest Radiographs: Computer-Aided
                  Classification September 13, 2021 Deep Learning for Chest
                  Radiographs: Computer-Aided Classification English | 2021 |
                  ISBN: 978-0323901840 | 228 Pages | PDF, EPUB | 103
                </Text>
              </View>
              <TouchableOpacity
                disabled={false}
                style={{
                  alignSelf: 'flex-end',
                  marginRight: (getWindowHeight() * 1) / 100,
                }}
                onPress={() => null}>
                <Image
                  source={res.images.clap_pressed}
                  style={{
                    width: (getWindowHeight() * 3) / 100,
                    height: (getWindowHeight() * 3) / 100,
                    margin: 5,
                    resizeMode: 'contain',
                  }}
                  resizeMode={'contain'}
                />

                <Text
                  style={[
                    styles.textWhiteMedium,
                    flashStyle.textFlashShadow,
                    {
                      color: res.colors.ColorGreen,

                      fontWeight: 'bold',
                    },
                  ]}>
                  500
                </Text>
              </TouchableOpacity>
            </LinearGradient>
          </FastImage>
        </View>
      </TouchableOpacity>

      {/* ////////////////////////////////////////////////////////////////////////////////////////////////////  */}

      <TouchableOpacity
        style={[flashStyle.aLaUneGrid, {marginHorizontal: 0}]}
        //onPress={() => this.shwoImage2()}
        onPress={() => null}>
        <FastImage
          source={{
            uri: 'https://apiv3.apifootball.com/badges/logo_leagues/300_copa-del-rey.png',
          }}
          style={{
            flexDirection: 'row',
            width: '100%',
            minHeight: 175,
          }}
          onLoadEnd={() => () => null}>
          <LinearGradient
            style={[flashStyle.aLaUneFiltre]}
            colors={['transparent', 'transparent', '#000']}>
            <View style={{width: getWindowWidth() - 90}}>
              <Text
                style={[
                  styles.textGreenSmall,
                  {
                    fontWeight: 'bold',
                  },
                ]}>
                15/02/2021
              </Text>
              <Text
                style={[
                  styles.textTitleWhite,
                  {
                    fontWeight: 'bold',
                  },
                ]}>
                rija
              </Text>
              <Text
                style={[
                  styles.textWiteSmall,
                  {
                    fontWeight: 'bold',
                  },
                ]}>
                qporgort
              </Text>
            </View>
            <TouchableOpacity
              disabled={false}
              style={{
                alignSelf: 'flex-end',
                marginRight: (getWindowHeight() * 1) / 100,
                width: (getWindowHeight() * 3) / 100,
              }}
              onPress={() => null}>
              <Image
                source={res.images.clap}
                style={{
                  width: (getWindowHeight() * 3) / 100,
                  height: (getWindowHeight() * 3) / 100,
                  margin: 5,
                  resizeMode: 'contain',
                }}
                resizeMode={'contain'}
              />

              <Text
                style={[
                  styles.textWhiteMedium,
                  flashStyle.textFlashShadow,
                  {
                    color: res.colors.ColorGreen,
                    fontWeight: 'bold',
                  },
                ]}>
                400
              </Text>
            </TouchableOpacity>
          </LinearGradient>
        </FastImage>
      </TouchableOpacity>
    </View>
  );
}
