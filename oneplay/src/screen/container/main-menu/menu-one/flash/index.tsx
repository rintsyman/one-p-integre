import React from 'react';
import {
  Text,
  View,
  ImageBackground,
  ActivityIndicator,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  TouchableWithoutFeedback,
  Keyboard,
  TextInput,
} from 'react-native';
import {HeaderLogoCenter} from '../../../../components/header-main/index';
import BackGroungTheme from '../../../../widget/back-groung-theme';
import res from '../../../../../resources/index';
import {styles, flashStyle} from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import CardMatch from './card-match/index';
import CardInOut from './bloc-in-out';
import FlashPlayer from './item-flash-player';
import ItemFlashAlaUne from './item-flash-a-la-une';
import Commentaire from './iem-commentaire';
export default function Index() {
  return (
    <BackGroungTheme>
      <View style={{flex: 1, paddingBottom: 25}}>
        <HeaderLogoCenter />
        <KeyboardAwareScrollView
          contentContainerStyle={{flexGrow: 1}}
          keyboardShouldPersistTaps="always"
          extraHeight={200}>
          <ScrollView
            keyboardShouldPersistTaps="always"
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{flexGrow: 1}}
            style={{flex: 1}}>
            <TouchableWithoutFeedback
              onPress={() => Keyboard.dismiss()}
              style={[styles.wpContent]}>
              <View
                style={{
                  paddingLeft: 18,
                  paddingRight: 18,
                  flexDirection: 'column',
                  justifyContent: 'flex-start',
                  alignItems: 'stretch',
                  flex: 1,
                  width: '100%',
                  height: '100%',
                }}>
                {/* ******************************************  MATCH LIVE  ******************************************** */}
                <CardMatch />

                {/* ***********************************************  IN / OUT  *********************************************** */}

                <CardInOut />

                {/* **************************************  BUTTON QUIZZ / COMPOS  **************************************** */}
                <View style={flashStyle.tabbedButton}>
                  <View style={flashStyle.tabbedButtonBack} />
                  <TouchableOpacity
                    style={flashStyle.gridButton1}
                    onPress={() => {}}>
                    <Text style={styles.textGrayTitle}>Composition</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={flashStyle.gridButton2}
                    onPress={() => null}>
                    <Text style={styles.textGreenTitle}>Statistiques</Text>
                  </TouchableOpacity>
                </View>

                {/* ****************************************** FLASH MES PLAYERS  ******************************************** */}
                <View
                  style={{
                    marginTop: 30,
                    marginBottom: 5,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <Text style={styles.textTitleWhite}>
                    Flashs de mes players
                  </Text>

                  <TouchableOpacity onPress={() => null}>
                    <Text style={styles.textGreenMedium}>Voir plus</Text>
                  </TouchableOpacity>
                </View>

                <FlashPlayer />

                {/* ******************************************  FLASH A LA UNE  ******************************************** */}
                <View
                  style={{
                    marginTop: 20,
                    minHeight: 30,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    paddingVertical: 10,
                  }}>
                  <Text style={styles.textTitleWhite}>Flashs à la une</Text>
                  <TouchableOpacity onPress={() => null}>
                    <Text style={styles.textGreenMedium}>Flashs à la une</Text>
                  </TouchableOpacity>
                </View>
                <ItemFlashAlaUne />

                <View
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    marginTop: 15,
                  }}>
                  <View
                    style={{
                      flex: 0.2,
                      flexDirection: 'column',
                      paddingTop: 10,
                      paddingBottom: 5,
                      paddingHorizontal: 5,
                    }}>
                    <Text style={styles.textTitleWhite}>Supporter</Text>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <View style={[styles.commentCount]}>
                        <View
                          style={[
                            styles.textLive,
                            {
                              marginTop: 0,
                              marginLeft: 0,
                            },
                          ]}>
                          <Text style={styles.textLiveIn}>LIVE</Text>
                        </View>
                        <View
                          style={{
                            height: '100%',
                            width: 1,
                            backgroundColor: 'gray',
                          }}
                        />
                        <Text style={[styles.textGraySmall, {marginLeft: 10}]}>
                          + 15 commentaires
                        </Text>
                      </View>
                      <TouchableOpacity onPress={() => null}>
                        <Text style={[styles.textGreenMedium]}>Voir plus</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
                <Commentaire />
              </View>
            </TouchableWithoutFeedback>
          </ScrollView>
        </KeyboardAwareScrollView>
      </View>
    </BackGroungTheme>
  );
}
