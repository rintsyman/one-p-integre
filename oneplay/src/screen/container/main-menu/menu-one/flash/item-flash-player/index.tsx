import React from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';

import res from '../../../../../../resources/index';
import {styles, flashStyle} from '../styles';
import {
  getWindowHeight,
  getWindowWidth,
} from '../../../../../components/header';
import FastImage from 'react-native-fast-image';
import LinearGradient from 'react-native-linear-gradient';

export default function Index() {
  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        height: (getWindowHeight() * 24) / 70,
        paddingHorizontal: 0,
        marginTop: 5,
      }}>
      <TouchableOpacity
        style={[
          flashStyle.playersGrid,

          {
            marginTop: 0,
            width: getWindowWidth() / 2 - 55,
            position: 'relative',
          },
        ]}
        onPress={_ => null}>
        <FastImage
          source={{
            uri: 'https://scanlibs.com/wp-content/uploads/deep-learning-chest-radiographs.jpg',
          }}
          style={{
            flexDirection: 'row',
            width: '100%',
          }}
          resizeMode="contain"
          onLoadEnd={() => null}>
          <LinearGradient
            style={{
              width: '100%',
              height: '100%',
              flex: 1,
              opacity: 0.9,
              flexDirection: 'column',
              justifyContent: 'space-around',
              alignItems: 'center',
            }}
            colors={[
              'transparent',
              'rgba(0, 0, 0, 0.05)',
              'rgba(0, 0, 0, 0.1)',
            ]}
            locations={[0, 0.3, 1]}>
            <View>
              <View style={[styles.profilePicMedium, {overflow: 'hidden'}]}>
                <FastImage
                  source={{
                    uri: 'https://scanlibs.com/wp-content/uploads/azure-data-fundamentals-certification.jpg',
                  }}
                  resizeMode={'cover'}
                  style={{
                    width: '100%',
                    height: '100%',
                    borderRadius: 50 / 2,
                    backgroundColor: 'white',
                  }}
                />
              </View>
              <Text style={[styles.textWhiteMedium, {fontWeight: 'bold'}]}>
                Tovo
              </Text>
            </View>
            <TouchableOpacity
              disabled
              style={{
                alignSelf: 'flex-end',
                marginRight: (getWindowHeight() * 1) / 100,
              }}>
              <Image
                source={res.images.clap}
                style={{
                  display: 'none',
                  width: (getWindowHeight() * 3) / 100,
                  height: (getWindowHeight() * 3) / 100,
                  margin: 5,
                  resizeMode: 'contain',
                }}
                resizeMode={'contain'}
              />

              <Text
                style={[
                  styles.textWhiteMedium,
                  flashStyle.textFlashShadow,
                  {
                    display: 'none',
                    color: res.colors.ColorGreen,
                  },
                ]}>
                25
              </Text>
            </TouchableOpacity>
          </LinearGradient>
        </FastImage>
      </TouchableOpacity>
    </View>
  );
}
