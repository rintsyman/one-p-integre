import React, {useState} from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';

import res from '../../../../../../resources/index';
import {styles} from '../styles';
import {getWindowHeight} from '../../../../../components/header';
import FastImage from 'react-native-fast-image';
import Modal from '../modal-statistic/index';
export default function Index() {
  const [isShowModal, setIsShowModal] = useState<boolean>(false);
  return (
    <View
      style={{
        marginLeft: -15,
        marginRight: -15,
        paddingRight: 15,
        paddingLeft: 15,
        position: 'relative',
      }}>
      <View
        style={[
          styles.gridMatch,
          {
            minHeight: (getWindowHeight() * 26) / 100,
            overflow: 'visible',
          },
        ]}>
        <View
          style={[
            styles.textLive,
            {
              backgroundColor: res.colors.ColorGrey,
            },
          ]}>
          <Text style={styles.textLiveIn}>LIVE</Text>
        </View>

        <View
          style={[
            styles.gridMatchContent,
            {position: 'relative', overflow: 'visible'},
          ]}>
          <View style={styles.equipe}>
            <View style={styles.logoEquipe1}>
              <View
                style={[
                  styles.choixEquipe,
                  {backgroundColor: res.colors.ColorGreen},
                ]}
              />
              <TouchableOpacity onPress={() => {}}>
                <FastImage
                  style={[styles.drapeauEquipe, {opacity: 1}]}
                  source={{
                    uri: 'https://apiv3.apifootball.com/badges/logo_leagues/383_super-cup.png',
                  }}
                />
              </TouchableOpacity>
            </View>

            <View
              style={[
                styles.score,
                {
                  justifyContent: 'flex-end',
                  paddingRight: 15,
                },
              ]}>
              <TouchableOpacity onPress={() => setIsShowModal(true)}>
                <Text style={styles.yellowCard}>0</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => null}>
                <Text style={styles.redCard}>0</Text>
              </TouchableOpacity>
              <Text style={styles.scoreNumber}>2</Text>
            </View>
          </View>

          <View style={styles.matchShowdown}>
            <Text style={{fontSize: 14, color: res.colors.ColorGreen}}>
              12:05
            </Text>
            <View
              style={[
                styles.versus,
                {
                  alignContent: 'center',
                  justifyContent: 'center',
                  alignItems: 'center',
                },
              ]}>
              <Text
                style={{
                  fontSize: 12,
                  color: res.colors.ColorWhite,
                }}>
                VS
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                top: 5,
              }}>
              <Image
                source={res.images.vues}
                style={{
                  height: 25,
                  width: 25,
                  resizeMode: 'contain',
                  marginRight: 10,
                }}
              />
              <Text style={[styles.textWhiteStyle12, {color: 'white'}]}>
                +200
              </Text>
            </View>
          </View>

          <View style={styles.equipe}>
            <View style={styles.logoEquipe2}>
              <TouchableOpacity onPress={() => {}}>
                <FastImage
                  style={[styles.drapeauEquipe, {opacity: 1}]}
                  source={{
                    uri: 'https://apiv3.apifootball.com/badges/logo_leagues/300_copa-del-rey.png',
                  }}
                />
              </TouchableOpacity>
              <View
                style={[
                  styles.choixEquipe,
                  {backgroundColor: res.colors.ColorGreyH},
                ]}
              />
            </View>
            <View
              style={[
                styles.score,
                {justifyContent: 'flex-start', paddingLeft: 15},
              ]}>
              <Text style={styles.scoreNumber}>2</Text>
              <TouchableOpacity onPress={() => null}>
                <Text style={styles.yellowCard}>0</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => null}>
                <Text style={styles.redCard}>0</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
      <Modal
        isModalShow={isShowModal}
        closeModal={() => setIsShowModal(false)}
      />
    </View>
  );
}
