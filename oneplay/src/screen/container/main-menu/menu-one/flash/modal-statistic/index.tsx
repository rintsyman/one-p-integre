import React from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import Modal from 'react-native-modal';
import res from '../../../../../../resources/index';
import FastImage from 'react-native-fast-image';
import {styles} from './style';
export type IProps = {
  isModalShow: boolean;
  closeModal: () => void;
};
export default function index(props: IProps) {
  return (
    <Modal
      isVisible={props.isModalShow}
      onBackButtonPress={() => props.closeModal()}
      backdropColor={'transparent'}
      style={styles.modalPop}
      animationInTiming={900}
      animationOutTiming={900}
      animationIn={'slideInUp'}
      animationOut={'slideOutDown'}>
      <ScrollView style={styles.containerStat}>
        <View style={styles.centerLive}>
          <View
            style={[
              styles.textLive,
              {
                backgroundColor: 'red',
              },
            ]}>
            <View style={styles.point}></View>
            <Text style={styles.textLiveIn}>LIVE</Text>
          </View>
        </View>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
            paddingBottom: 25,
          }}>
          <Text style={styles.txtChampion}>Champion Leagues</Text>
        </View>
        <View style={styles.containerLogo}>
          <View style={styles.leftEquipe}>
            <FastImage
              style={[styles.drapeauEquipe, {opacity: 1}]}
              source={{
                uri: 'https://apiv3.apifootball.com/badges/logo_leagues/383_super-cup.png',
              }}
            />
            <Text style={styles.txtEquipe}>Barcelona</Text>
          </View>
          <View style={styles.centerEquipe}>
            <Text style={styles.txtScore}>2 : 0</Text>
            <Text style={styles.txtTime}>81`</Text>
          </View>
          <View style={styles.rigthEquipe}>
            <FastImage
              style={[styles.drapeauEquipe, {opacity: 1}]}
              source={{
                uri: 'https://apiv3.apifootball.com/badges/logo_leagues/300_copa-del-rey.png',
              }}
            />
            <Text style={styles.txtEquipe}>PSG</Text>
          </View>
        </View>
        <View style={styles.containerInfo}>
          <View style={styles.bordered}></View>
        </View>
      </ScrollView>
    </Modal>
  );
}
