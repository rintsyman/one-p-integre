import res from '../../../../../../resources/index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  modalPop: {
    margin: 0,
    alignItems: 'center',
    backgroundColor: 'transparent',
    height: 250,
    position: 'relative',
  },
  containerStat: {
    height: '60%',
    width: '100%',
    backgroundColor: 'white',
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  centerLive: {
    paddingTop: 15,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textLive: {
    minWidth: 50,
    height: 20,
    alignSelf: 'flex-start',
    alignContent: 'center',
    justifyContent: 'space-around',
    backgroundColor: '#ff4405',
    color: 'white',
    marginTop: 5,
    borderRadius: 4,
    textAlign: 'center',
    fontSize: 11,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textLiveIn: {
    color: 'white',
    textAlign: 'center',
    fontSize: 11,
  },
  point: {
    backgroundColor: 'white',
    height: 10,
    width: 10,
    borderRadius: 5,
  },
  txtChampion: {
    top: 10,
    color: res.colors.ColorBlack,
    fontSize: 25,
    fontWeight: 'bold',
    fontFamily: res.fonts.AvenirMedium,
  },
  containerLogo: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    flex: 1,
  },
  leftEquipe: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  centerEquipe: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rigthEquipe: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  drapeauEquipe: {
    alignItems: 'flex-end',
    width: 80,
    height: 80,
    borderRadius: 50 / 2,
  },
  txtEquipe: {
    fontSize: 18,
    fontFamily: res.fonts.AvenirBlack,
    fontWeight: 'bold',
  },
  txtScore: {
    fontSize: 35,
    fontFamily: res.fonts.AvenirBlack,
    fontWeight: 'bold',
  },
  txtTime: {
    fontSize: 20,
    fontFamily: res.fonts.AvenirBlack,
    fontWeight: 'bold',
  },
  containerInfo: {
    width: '100%',
    flex: 1,
  },
  bordered: {
    borderWidth: 2,
    height: 100,
    marginHorizontal: 15,
    borderRadius: 10,
    marginTop: 15,
    borderColor: res.colors.ColorGreyO25,
  },
});
