import React from 'react';
import {Text, View, TouchableOpacity, Image, TextInput} from 'react-native';
import res from '../../../../../../resources/index';
import {styles, flashStyle} from '../styles';
import {
  getWindowHeight,
  getWindowWidth,
} from '../../../../../components/header';
import FastImage from 'react-native-fast-image';

export default function Index() {
  const geth6100 = (getWindowHeight() * 6) / 100;
  const geth3100 = (getWindowHeight() * 3) / 100;
  const getw70100 = (getWindowWidth() * 70) / 100;
  const getw62100 = (getWindowWidth() * 62) / 100;
  const getw58100 = (getWindowHeight() * 5.8) / 100;
  const getw55100 = (getWindowWidth() * 55) / 100;

  return (
    <View style={[, {paddingVertical: 0}]}>
      <TouchableOpacity
        style={{
          backgroundColor: '#2B2F4E',
          borderRadius: 20,
          marginStart: 0,
          marginEnd: 5,
          marginVertical: 15,
        }}
        onPress={() => {}}
        onLongPress={e => {}}>
        <View style={[styles.commentGrid, {paddingHorizontal: 5}]}>
          <View
            style={{
              width: 24,
              height: 24,
              marginTop: 15,
              borderWidth: 1,
              borderColor: res.colors.ColorGreen,
              borderRadius: geth3100,
              overflow: 'hidden',
              marginRight: 15,
              display: 'flex',
              backgroundColor: 'white',
            }}
          />

          <View
            style={[
              styles.profilePicSmall,
              {
                display: 'flex',
                alignContent: 'center',
                alignItems: 'center',
                justifyContent: 'center',
              },
            ]}>
            <FastImage
              resizeMode="cover"
              style={{
                alignSelf: 'center',
                width: geth6100,
                height: 50,
                // backgroundColor: 'red',
              }}
              source={{
                uri: 'https://scanlibs.com/wp-content/uploads/azure-data-fundamentals-certification.jpg',
              }}
            />
          </View>
          <View
            style={{
              alignContent: 'flex-start',
              alignItems: 'flex-start',
            }}>
            <Text style={styles.textGreenTitle}>Tiv</Text>

            <Text
              style={[
                styles.textGrayMedium,
                {
                  textAlign: 'left',
                  width: getw70100,
                  paddingRight: 11,
                  marginRight: 22,
                  maxWidth: '85%',
                },
              ]}>
              qsqsqsq sqdsd
            </Text>

            <Text
              style={{
                fontSize: 11,
                fontWeight: '900',
                paddingRight: 5,
                color: res.colors.ColorWhiteO5,
                // backgroundColor: 'red',
              }}>
              12:25
            </Text>
          </View>
        </View>
      </TouchableOpacity>

      <View
        style={[
          styles.commentGrid,
          {
            flex: 1,
            width: '100%',
            paddingBottom: 10,
            borderRadius: 20,
            borderColor: res.colors.ColorGreen,
            alignItems: 'center',
            borderWidth: 1,
          },
        ]}>
        <View style={styles.inputContainer}>
          <TextInput
            style={[
              styles.textInput,
              {
                display: 'flex',
              },
            ]}
            placeholder={'Commentaire'}
            placeholderTextColor="gray"
            onChangeText={text => null}
            returnKeyType={'next'}
            onSubmitEditing={() => {}}
            blurOnSubmit={true}
            multiline={true}
            scrollEnabled={true}
          />
        </View>

        <View>
          <TouchableOpacity
            onPress={() => null}
            style={{
              backgroundColor: res.colors.ColorGreen,
              padding: 10,
              width: geth6100,
              height: geth6100,
              borderRadius: 50,
              alignItems: 'center',
              alignContent: 'center',
              justifyContent: 'center',
              marginRight: 5,
            }}>
            <Image
              source={res.images.flesh_black}
              style={{
                width: 20,
                height: 20,
                resizeMode: 'contain',
              }}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}
