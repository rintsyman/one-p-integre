import React from 'react';
import {
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  Image,
} from 'react-native';

import res from '../../../../../../resources/index';
import {styles} from '../styles';
import {
  getWindowHeight,
  getWindowWidth,
} from '../../../../../components/header';

export default function Index() {
  return (
    <View
      style={[styles.gridInOut, {minHeight: (getWindowHeight() * 15) / 100}]}>
      <ImageBackground
        source={res.images.in}
        style={{
          flexDirection: 'row',
          width: '100%',
          height: '100%',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <TouchableOpacity
          style={{marginLeft: (getWindowWidth() * 5) / 100}}
          onPress={() => null}>
          <Image
            source={res.images.arrow_left}
            style={{resizeMode: 'contain', width: 25}}
          />
        </TouchableOpacity>
        <Text style={styles.textMatch}>IN</Text>
        <View
          style={{
            marginRight: (getWindowWidth() * 5) / 100,
            width: 20,
          }}
        />
      </ImageBackground>
    </View>
  );
}
