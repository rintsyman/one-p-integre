import React from 'react';
import {
  Text,
  View,
  ImageBackground,
  ActivityIndicator,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  TouchableWithoutFeedback,
  Keyboard,
  TextInput,
} from 'react-native';
import {
  getWindowHeight,
  getWindowWidth,
} from '../../../../../components/header';
import LinearGradient from 'react-native-linear-gradient';
import res from '../../../../../../resources/index';
import FastImage from 'react-native-fast-image';
import {styles} from './style';
import AttachmentIcon from 'react-native-vector-icons/Entypo';
import {HeaderLogoCenter} from '../../../../../components/header-main/index';
import BackGroungTheme from '../../../../../widget/back-groung-theme';

export default function Index() {
  return (
    <BackGroungTheme>
      <View
        style={[
          styles.WrapperBg,
          {
            flex: 1,
            justifyContent: 'flex-end',
          },
        ]}>
        <HeaderLogoCenter />

        <FastImage
          style={[
            styles.fond,
            {
              position: 'relative',
              height: '85%',
              width: '100%',
            },
          ]}
          source={{
            uri: 'https://i.pinimg.com/564x/e5/47/3d/e5473df5b70235240c1d3807ae60ff0f.jpg',
          }}
          resizeMode={'cover'}
          onLoadEnd={() => null}>
          <LinearGradient
            style={{
              flexDirection: 'column',
              position: 'relative',
              width: getWindowWidth(),
              flex: 1,
            }}
            colors={['transparent', 'transparent', '#000']}>
            <View style={{flex: 4}} />

            <View style={[styles.profil, {height: 'auto'}]}>
              <View
                style={{
                  flexDirection: 'column',
                  marginTop: 10,
                  marginStart: 10,
                  flex: 0.9,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                  }}>
                  <TouchableOpacity
                    style={{
                      flex: 0.1,
                      marginTop: 20,
                      justifyContent: 'center',
                    }}>
                    <FastImage
                      source={{
                        uri: 'https://scanlibs.com/wp-content/uploads/android-development-retrofit.jpg',
                      }}
                      style={{
                        width: 40,
                        height: 40,
                        marginLeft: 2,
                        borderRadius: 40,
                      }}
                    />
                  </TouchableOpacity>

                  <View
                    style={{
                      flexDirection: 'column',
                      paddingLeft: 40,
                      paddingTop: 35,
                    }}>
                    <Text style={styles.textGreenSmall}>12/12/2021</Text>

                    <TouchableOpacity onPress={() => null}>
                      <Text
                        style={[
                          styles.textTitleWhite,

                          {
                            marginVertical: 10,
                            fontWeight: 'bold',
                          },
                        ]}>
                        Jose
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>

              <View>
                <TouchableOpacity
                  style={{
                    alignSelf: 'flex-end',
                    marginRight: 10,
                    marginTop: 40,
                  }}
                  onPress={() => null}>
                  <Image
                    source={res.images.clap}
                    style={{
                      width: (getWindowHeight() * 3) / 100,
                      height: (getWindowHeight() * 3) / 100,
                      margin: 5,
                      resizeMode: 'contain',
                    }}
                    resizeMode={'contain'}
                  />
                </TouchableOpacity>
                <Text
                  style={[
                    styles.textWhiteMedium,
                    {
                      marginRight: 15,
                      color: res.colors.ColorGreen,
                    },
                  ]}>
                  25
                </Text>
              </View>
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                paddingHorizontal: 10,
              }}>
              <Text
                numberOfLines={3}
                style={[
                  styles.textWiteSmall,
                  {
                    overflow: 'hidden',
                    height: 'auto',
                  },
                ]}>
                1080x1920 Portrait, blur, beautiful sunset, seascape wallpaper |
                Beautiful wallpaper for phone, Oneplus wallpapers, Sunset Visit
                Images may be subject to copyright. Learn More flex: 1,
                alignItems: 'center', backgroundColor: 'blue',
                paddingHorizontal: 10
              </Text>
            </View>
          </LinearGradient>
        </FastImage>
      </View>
    </BackGroungTheme>
  );
}
