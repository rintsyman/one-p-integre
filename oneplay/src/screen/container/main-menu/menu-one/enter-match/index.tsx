import React from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import {HeaderFond} from '../../../../components/header-fond/index';
import BackGroungTheme from '../../../../widget/back-groung-theme';
import res from '../../../../../resources/index';
import {styles} from './style';
import {getWindowWidth} from '../../../../components/header';
import UseEnterMatch from '../../../../../controller/main-menu-flow-ctr/menu-one-ctr/use-enter-match';
export default function Index() {
  const providerEnterMatch = {...UseEnterMatch()};
  return (
    <BackGroungTheme>
      <View style={{flex: 1}}>
        <View>
          <HeaderFond />
        </View>
        <View style={{flex: 1}}>
          <View style={styles.viexCntainer}>
            <View style={styles.questionGrid}>
              <Text style={[styles.textTitleWhite, {fontSize: 16}]}>
                Quelle équipe supportes-tu?
              </Text>
            </View>
            <View style={[styles.homeBoxChoixCtn, {}]}>
              <View style={[styles.homeBlock]}>
                <View
                  style={[
                    styles.block,
                    {
                      opacity: 1,
                      alignContent: 'flex-start',
                      alignItems: 'flex-start',
                    },
                  ]}>
                  <TouchableOpacity onPress={() => {}}>
                    <Image
                      style={[
                        styles.imageOneStyle,
                        {
                          maxHeight: 120,
                          zIndex: 1,
                          maxWidth: getWindowWidth() / 5,
                        },
                      ]}
                      source={{
                        uri: 'https://apiv3.apifootball.com/badges/logo_leagues/300_copa-del-rey.png',
                      }}
                    />
                    <Image
                      source={res.images.cheched}
                      style={[styles.checkBoxImage, {opacity: 1}]}
                    />
                  </TouchableOpacity>
                  <Text
                    style={[
                      styles.textWhiteStyle12,
                      {color: res.colors.ColorGreen},
                    ]}>
                    Barea
                  </Text>
                </View>

                <View style={styles.viewCenter}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Image
                      source={res.images.vues}
                      style={{
                        height: 30,
                        width: 30,
                        resizeMode: 'contain',
                        marginRight: 10,
                      }}></Image>
                    <Text
                      style={{
                        color: 'white',
                        marginTop: 5,
                        fontFamily: res.fonts.AvenirMedium,
                      }}>
                      200
                    </Text>
                  </View>
                  <View>
                    <Text
                      style={{
                        color: 'gray',
                        fontFamily: res.fonts.AvenirMedium,
                      }}>
                      12/1/2021
                    </Text>
                  </View>
                  <View>
                    <Text
                      style={{
                        color: 'gray',
                        marginTop: 10,
                        fontFamily: res.fonts.AvenirMedium,
                      }}>
                      {/* {this.props.match_time} */}
                      12:00
                    </Text>
                  </View>
                </View>

                <View style={[styles.block2, {opacity: 1}]}>
                  <TouchableOpacity onPress={() => {}}>
                    <Image
                      style={[
                        styles.imageTwoStyle,
                        {maxHeight: 120, zIndex: 1},
                      ]}
                      source={{
                        uri: 'https://apiv3.apifootball.com/badges/logo_leagues/383_super-cup.png',
                      }}
                    />
                    <Image
                      source={res.images.cheched}
                      style={[styles.checkBoxImage, {opacity: 0.5}]}
                    />
                  </TouchableOpacity>
                  <Text
                    style={[
                      styles.textWhiteStyle12,
                      {color: res.colors.ColorWhite},
                    ]}>
                    Zambie
                  </Text>
                </View>
              </View>
            </View>

            <TouchableOpacity
              style={[styles.btnGreeBig, styles.btnGo]}
              onPress={() => providerEnterMatch.goInOut()}>
              <Text
                style={[
                  styles.btnGreeBigTxt,
                  {
                    textAlign: 'center',
                    fontSize: 16,
                    fontFamily: res.fonts.AvenirMedium,
                  },
                ]}>
                GO
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </BackGroungTheme>
  );
}
