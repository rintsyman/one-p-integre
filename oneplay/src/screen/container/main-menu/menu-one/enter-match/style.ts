/*************Import from react lib********************/
import {StyleSheet, Dimensions} from 'react-native';

/*************Import from source code********************/
import res from '../../../../../resources/index';
import {getWindowWidth} from '../../../../../utils/utils';
import {getWindowHeight} from '../../../../components/header';
export const styles = StyleSheet.create({
  viexCntainer: {
    flex: 1,
  },
  questionGrid: {
    marginHorizontal: (getWindowWidth() * 4) / 100,
    paddingVertical: (getWindowWidth() * 5) / 100,
    marginBottom: (getWindowWidth() * 4) / 100,
    borderBottomColor: res.colors.ColorGreen,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  textTitleWhite: {
    fontFamily: res.fonts.AvenirMedium,
    fontSize: 18,
    color: res.colors.ColorWhite,
    lineHeight: 20,
  },
  homeBoxChoixCtn: {
    borderRadius: 16,
    borderWidth: 1,
    borderColor: res.colors.ColorGreen,
    backgroundColor: 'rgba(76, 192, 255, .1 )',
    flexDirection: 'column',
    alignContent: 'flex-start',
    alignItems: 'flex-start',
    margin: 15,
    minHeight: getWindowHeight() / 4,
  },
  homeBlock: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 10,
    minHeight: getWindowHeight() / 6,
  },
  block: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    resizeMode: 'contain',
    marginLeft: 20,
    marginRight: 20,
  },
  imageOneStyle: {
    flex: 1,
    height: 60,
    width: 75,
    resizeMode: 'contain',
  },
  checkBoxImage: {
    width: 25,
    height: 25,
    position: 'absolute',
    zIndex: 10,
    bottom: 0,
    right: 0,
  },
  textWhiteStyle12: {
    fontFamily: res.fonts.AvenirMedium,
    fontSize: 12,
    color: res.colors.ColorWhite,
  },
  viewCenter: {
    flex: 1.5,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    alignContent: 'stretch',
  },
  block2: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 20,
    marginRight: 20,
  },
  imageTwoStyle: {
    flex: 1,
    resizeMode: 'contain',
    height: 60,
    width: 75,
  },
  btnGreeBig: {
    backgroundColor: res.colors.ColorGreen,
    borderRadius: 24,
    height: 48,
    minWidth: '85%',
    color: res.colors.ColorBlack,
    marginBottom: 10,
    marginTop: 10,
  },
  btnGreeBigTxt: {
    color: res.colors.ColorBlack,
    alignItems: 'center',
    alignContent: 'center',
    lineHeight: 25,
    fontSize: 12,
  },
  btnGo: {
    marginHorizontal: (getWindowWidth() * 10) / 100,
    alignSelf: 'center',
    justifyContent: 'center',
  },
});
