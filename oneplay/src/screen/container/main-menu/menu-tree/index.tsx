import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  FlatList,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {styles} from './style';
import res from '../../../../resources/index';
import {baseStyle} from '../../../../resources/style/base';
import {BoutonWidget} from '../../../widget/bouton-cusom';
import WidgetTextInput from '../../../widget/input-text';
import RadioButton from '../../../widget/radioBouton';
import Divider from '../../../widget/divider-row';
import {HeaderLogoCenter} from '../../../components/header-main/index';
import BackGroungTheme from '../../../widget/back-groung-theme';

export default function Tchat() {
  let data = Array.from(Array(20).keys());

  return (
    <BackGroungTheme>
      <View style={{flex: 1}}>
        <HeaderLogoCenter />
        <TouchableOpacity
          onPress={() => {}}
          activeOpacity={1}
          style={{marginHorizontal: 5, paddingVertical: 5}}>
          <WidgetTextInput
            icone={null}
            placeholder={'Rechercher'}
            customTextInputStyle={{color: 'white'}}>
            <Image
              source={res.images.find}
              style={[{width: 20, height: 20}, styles?.arrowIcon]}
            />
          </WidgetTextInput>
        </TouchableOpacity>
        <FlatList
          data={data}
          keyExtractor={item => item.toString()}
          initialNumToRender={9}
          maxToRenderPerBatch={9}
          ItemSeparatorComponent={() => <Divider customStyle={{left: 100}} />}
          renderItem={element => (
            <View style={styles.containerItem}>
              <View style={styles.containerImage}>
                <Image
                  style={styles?.roundedProfil}
                  source={res.images.profil}
                />
              </View>
              <TouchableOpacity style={styles?.containerTxt}>
                <Text
                  style={[
                    baseStyle?.smallText,
                    {color: res.colors.ColorGreen, fontWeight: 'bold'},
                  ]}>
                  Nom et Prenom
                </Text>
                <Text
                  style={[baseStyle?.smallText, styles?.customMess]}
                  numberOfLines={2}>
                  Web integration is one of the foundational pillars of a good
                  mobile app. But it doesn’t mean you have to write a lot of
                  code. Square offers a free, open-source library called
                  RetroFit that make
                </Text>
                <Text style={[baseStyle?.smallText, styles?.styledDate]}>
                  a l'instant
                </Text>
              </TouchableOpacity>
            </View>
          )}
          windowSize={9}
        />
      </View>
    </BackGroungTheme>
  );
}
