import {Dimensions, StyleSheet} from 'react-native';
import res from '../../../../resources/index';

export const styles = StyleSheet.create({
  arrowIcon: {
    position: 'absolute',
    right: 15,
  },
  containerItem: {
    height: Dimensions.get('window').height / 7,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: 5,
  },
  containerImage: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 20,
    width: 80,
  },
  containerTxt: {
    justifyContent: 'flex-start',
    flexDirection: 'column',
    alignItems: 'baseline',
    top: 10,
    left: 15,
    flexShrink: 1,
    marginRight: 15,
    height: Dimensions.get('window').height / 9,
  },

  roundedProfil: {
    width: 55,
    height: 55,
    borderRadius: 55 / 2,
  },
  customMess: {
    fontFamily: res.fonts.AvenirLigth,
    flexShrink: 1,
    right: 0,
    color: 'white',
  },
  styledDate: {
    position: 'absolute',
    top: 0,
    right: 25,
    fontFamily: res.fonts.AvenirLigth,
    color: 'white',
    fontWeight: 'bold',
    fontSize: 10,
  },
});
