import React from 'react';

import {View, Text, Image, TouchableOpacity, FlatList} from 'react-native';
import Theme from '../../../widget/back-groung-theme';
import HeaderSearchCenter from '../../../components/header-search-center/index';
import Modal from 'react-native-modal';
type Iprops = {
  modalVisible: boolean;
  closeModal: () => void;
};

export default function Index(props: Iprops) {
  return (
    <Modal
      isVisible={props?.modalVisible}
      statusBarTranslucent
      coverScreen
      animationIn="slideInRight"
      animationOut="slideOutRight"
      animationInTiming={900}
      animationOutTiming={600}
      style={{
        margin: 0,
        alignItems: 'center',
      }}
      onBackButtonPress={() => props?.closeModal()}>
      <Theme>
        <View style={{flex: 1}}>
          <HeaderSearchCenter />
        </View>
      </Theme>
    </Modal>
  );
}
