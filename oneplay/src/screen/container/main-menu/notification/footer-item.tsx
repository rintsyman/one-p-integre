import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {baseStyle} from '../../../../resources/style/base';
import {styles} from './style';

export type Iprops = {
  label: string;
  seeMore: () => void;
};
export default function FooterItem(props: Iprops) {
  return (
    <TouchableOpacity
      style={[styles?.containerFooter]}
      onPress={() => props.seeMore()}>
      <Text style={[baseStyle?.smallText, styles.customTitleFooter]}>
        {props?.label}
      </Text>
    </TouchableOpacity>
  );
}
