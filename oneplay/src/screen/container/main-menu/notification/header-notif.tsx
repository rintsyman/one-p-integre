import React from 'react';
import {View, Text} from 'react-native';
import {baseStyle} from '../../../../resources/style/base';
import {styles} from './style';

export type Iprops = {
  label: string;
};
export default function HeaderNotif(props: Iprops) {
  return (
    <View style={[styles?.containerHeader]}>
      <Text style={[baseStyle?.smallText, styles.customTitle]}>
        {props?.label}
      </Text>
    </View>
  );
}
