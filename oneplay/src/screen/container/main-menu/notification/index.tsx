import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  FlatList,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {styles} from './style';
import res from '../../../../resources/index';
import {baseStyle} from '../../../../resources/style/base';
import {BoutonWidget} from '../../../widget/bouton-cusom';
import WidgetTextInput from '../../../widget/input-text';
import RadioButton from '../../../widget/radioBouton';
import Divider from '../../../widget/divider-row';
import HeaderNotif from './header-notif';
import FooterItem from './footer-item';
import {HeaderFond} from '../../../components/header-fond/index';
import ItemListeToday from './item-today-list';
import Theme from '../../../widget/back-groung-theme';
import UseNotification from '../../../../controller/main-menu-flow-ctr/menu-one-ctr/use-notif';
export default function Notification() {
  let data = Array.from(Array(2).keys());
  let encient = Array.from(Array(2).keys());
  const providerNotification = {...UseNotification()};
  return (
    <Theme>
      <View style={{flex: 1}}>
        <HeaderFond />

        <View style={styles?.containerNow}>
          <FlatList
            data={data}
            keyExtractor={item => item.toString()}
            initialNumToRender={9}
            maxToRenderPerBatch={9}
            ItemSeparatorComponent={() => <Divider />}
            ListHeaderComponent={() => <HeaderNotif label=" Aujourd'hui" />}
            renderItem={element => <ItemListeToday />}
            windowSize={9}
            ListFooterComponent={() => (
              <FooterItem
                label="Voir plus ..."
                seeMore={() => providerNotification.goAllNotif()}
              />
            )}
            scrollEnabled={false}
          />
        </View>

        <FlatList
          data={encient}
          keyExtractor={item => item.toString()}
          initialNumToRender={9}
          maxToRenderPerBatch={9}
          ItemSeparatorComponent={() => <Divider />}
          ListHeaderComponent={() => <HeaderNotif label="Ancient" />}
          renderItem={element => <ItemListeToday />}
          windowSize={9}
          ListFooterComponent={() => (
            <FooterItem
              label="Voir plus ..."
              seeMore={() => providerNotification.goAllNotif()}
            />
          )}
          scrollEnabled={false}
        />
      </View>
    </Theme>
  );
}
