import React, {Component} from 'react';
import {
  Text,
  View,
  ImageBackground,
  StyleSheet,
  ScrollView,
  FlatList,
} from 'react-native';
import {HeaderNotif} from '../../../../widget/header-notif';
import res from '../../../../../resources/index';

import {connect} from 'react-redux';

import {FlatNotif} from './item-all-notif';
import Theme from '../../../../widget/back-groung-theme';

import FastImage from 'react-native-fast-image';
let data = Array.from(Array(20).keys());

export const AllNotification = () => {
  const listeVide = () => {
    return (
      <View
        style={{
          //display: !this.state.loading ? 'flex' : 'none',
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: '60%',
          // height: '100%',
        }}>
        <Text
          style={{
            textAlign: 'center',
            color: res.colors.ColorGreen,
            fontFamily: res.fonts.AvenirMedium,
            fontSize: 20,
            alignSelf: 'center',
            flex: 1,
          }}>
          Vide
        </Text>
      </View>
    );
  };

  return (
    <View style={{flex: 1, backgroundColor: '#040635'}}>
      <Theme>
        <View style={{flex: 1}}>
          <HeaderNotif />
          <View
            style={{
              flex: 1,
              margin: 15,
            }}>
            <FlatList
              data={data}
              // inverted={true}
              keyExtractor={item => item.toString()}
              renderItem={({item, index}) => <FlatNotif />}
              ListEmptyComponent={listeVide()}
            />
          </View>
        </View>
      </Theme>
    </View>
  );
};
