import React, {Component} from 'react';
import {
  Text,
  View,
  ImageBackground,
  SafeAreaView,
  TouchableOpacity,
  Image,
  StyleSheet,
} from 'react-native';
import res from '../../../../../resources/index';
import FastImage from 'react-native-fast-image';
import {notifStyle} from './style';
export const FlatNotif = () => {
  return (
    <View>
      <Text
        style={{
          color: res.colors.ColorGreen,
          alignSelf: 'center',
          marginTop: 5,
          fontFamily: res.fonts.AvenirMedium,
        }}></Text>
      <View style={notifStyle.block}>
        <TouchableOpacity style={notifStyle.imageCircleContent}>
          <FastImage
            source={res.images.profil}
            style={notifStyle.imageCircleStyle}
          />
        </TouchableOpacity>
        <View style={notifStyle.details}>
          <TouchableOpacity style={[notifStyle.lableBox]} disabled={true}>
            <Text style={notifStyle.labels}>qsqsqsq</Text>
          </TouchableOpacity>
          <Text style={{color: res.colors.ColorGrey, marginTop: 10}}>
            Vous n'avez aucun player dans votre listeVous n'avez aucun player
            dans votre liste Vous n'avez aucun player dans votre liste
          </Text>
          <View style={notifStyle.buttons}>
            <TouchableOpacity
              style={notifStyle.buttonLink}
              onPress={() => null}>
              <Text style={notifStyle.labelsBtn}>Accepter</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={notifStyle.buttonLink}
              onPress={() => null}>
              <Text style={notifStyle.labelsBtn}>Refuser</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};
