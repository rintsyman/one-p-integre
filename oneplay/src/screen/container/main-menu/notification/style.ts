import {Dimensions, StyleSheet} from 'react-native';
import res from '../../../../resources/index';

export const styles = StyleSheet.create({
  containerNotif: {
    flex: 1,
    paddingHorizontal: 25,
  },
  customTitle: {
    fontFamily: res.fonts.AvenirMedium,
    fontSize: 18,
    paddingVertical: 10,
    color: res.colors.ColorGreen,
  },
  containerHeader: {
    left: 15,
    right: 15,
    marginTop: 5,
  },
  containerItemListe: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingVertical: 15,
  },
  containerItemLeft: {
    flexDirection: 'row',
    width: Dimensions.get('screen').width - 90,
    height: Dimensions.get('screen').height / 14,
    flexShrink: 1,
  },
  customTitleItem: {
    fontSize: 14,
    fontFamily: res.fonts.AvenirMedium,
    paddingLeft: 18,
    color: 'white',
  },
  containerNow: {
    height: Dimensions.get('screen').height / 2.8,
  },
  containerFooter: {
    left: 60,
    paddingBottom: 15,
  },
  customTitleFooter: {
    fontFamily: res.fonts.AvenirBlack,
    fontSize: 15,
    color: res.colors.ColorGreen,
  },
  containerTxt: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
  customDate: {
    fontSize: 14,
    fontFamily: res.fonts.AvenirBlack,
    paddingLeft: 18,
    paddingTop: 5,
    color: 'white',
  },
});
