import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacityBase,
  TouchableOpacity,
} from 'react-native';
import {baseStyle} from '../../../../resources/style/base';
import {styles} from './style';
import res from '../../../../resources/index';
import AttachmentIcon from 'react-native-vector-icons/Entypo';

export default function ItemTodayList() {
  return (
    <TouchableOpacity style={styles.containerNotif}>
      <View style={[styles.containerItemListe]}>
        <View style={[styles?.containerItemLeft]}>
          <Image
            source={res.images.notif}
            height={25}
            width={25}
            style={[baseStyle?.smallIcon]}
          />
          <View style={styles.containerTxt}>
            <Text
              style={[baseStyle?.smallText, styles?.customTitleItem]}
              numberOfLines={2}>
              WARNING:: The specified Android SDK Build Tools version (23.0.1)
              is ignored, as it is below the minimum supported version (30.0.2)
              for Android Gradle Plugin 4.2.1. Android SDK Build Tools 30.0.2
              will be used. To suppress this warning, remove "buildToolsVersion
              '23.0.1'" from your build.gradle file, as each version of the
              Android Gradle Plugin now has a default version of the build tools
              Task :app:installDebug Installing APK 'app-debug.apk' on 'VTR-L29
              - 9' for app:debug Installed on 1 device.
            </Text>
            <Text style={[baseStyle?.smallText, styles?.customDate]}>
              25/02/2022
            </Text>
          </View>
        </View>
        <AttachmentIcon
          size={25}
          name="cross"
          style={[baseStyle.smallIcon]}
          color={res.colors.ColorGreen}
        />
      </View>
    </TouchableOpacity>
  );
}
