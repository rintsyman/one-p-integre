import React, {useRef, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import BackGroungTheme from '../../../widget/back-groung-theme';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/Entypo';
import res from '../../../../resources/index';

export default function Index() {
  let refCamera: any = useRef();
  const [isFace, setIsFace] = useState<boolean>(false);
  const [isFlashActive, setisFlashActive] = useState<boolean>(false);
  const [isVideo, setisVideo] = useState<boolean>(false);

  const switchCapture = () => {
    setisVideo(!isVideo);
  };

  const takePicture = async () => {
    if (refCamera) {
      const options = {quality: 0.5, maxDuration: 25};
      const data = await refCamera?.takePictureAsync(options);
      console.log(data.uri);
    }
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.close}>
        <Icon name="cross" size={25} color={res.colors.ColorGrey} />
      </TouchableOpacity>
      <View style={styles.flash_reverse}>
        <TouchableOpacity onPress={() => setisFlashActive(!isFlashActive)}>
          <Icon
            name="flash"
            size={25}
            color={isFlashActive ? res.colors.ColorGreen : res.colors.ColorGrey}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setIsFace(!isFace)}>
          <Icon
            name="mobile"
            size={25}
            color={isFace ? res.colors.ColorGreen : res.colors.ColorGrey}
          />
        </TouchableOpacity>
      </View>

      <RNCamera
        ref={ref => {
          refCamera = ref;
        }}
        defaultVideoQuality={RNCamera.Constants.VideoQuality['480p']}
        style={styles.preview}
        type={
          isFace ? RNCamera.Constants.Type.front : RNCamera.Constants.Type.back
        }
        flashMode={RNCamera.Constants.FlashMode.on}
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
        androidRecordAudioPermissionOptions={{
          title: 'Permission to use audio recording',
          message: 'We need your permission to use your audio',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
      />
      <View style={styles?.containerTools}>
        <View
          style={{
            position: 'relative',
            justifyContent: 'center',
            alignItems: 'center',
          }}></View>
        <TouchableOpacity onPress={() => takePicture()} style={styles.capture}>
          {isVideo ? (
            <Icon name="controller-record" size={25} color={'red'} />
          ) : (
            <Icon name="camera" size={25} color={res.colors.ColorGreen} />
          )}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => switchCapture()}
          style={styles.captureRigth}>
          <Icon
            name={!isVideo ? 'video-camera' : 'camera'}
            size={18}
            color={res.colors.ColorGrey}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'transparent',
    position: 'relative',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    backgroundColor: 'white',
    borderWidth: 5,
    borderColor: res.colors.ColorGreen,
    borderRadius: 65 / 2,
    height: 65,
    width: 65,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    margin: 20,
  },

  captureRigth: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 35 / 2,
    height: 35,
    width: 35,
    backgroundColor: 'white',
    position: 'absolute',
    right: Dimensions.get('window').width / 6,
  },

  containerTools: {
    flex: 0,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
  },
  close: {
    position: 'absolute',
    left: 25,
    top: 25,
    zIndex: 5,
    borderRadius: 50 / 2,
    height: 50,
    width: 50,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  flash_reverse: {
    borderRadius: 50 / 2,
    height: 50,
    width: 100,
    backgroundColor: 'white',
    position: 'absolute',
    right: 25,
    top: 25,
    zIndex: 5,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
  },
});
