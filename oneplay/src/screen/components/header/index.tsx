import React from 'react';
import {
  Text,
  View,
  StatusBar,
  ActivityIndicator,
  ImageBackground,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {styles} from './styles';
import res from '../../../resources/index';
import {Header} from 'react-native-elements';

export const getWindowHeight = () => Dimensions.get('window').height;

export const getWindowWidth = () => Dimensions.get('window').width;

export default function Index() {
  /*************Render Events********************/
  const _backButton = () => {};
  /*************Render PARTS********************/

  const _renderIconLeft = () => {
    return (
      <View style={[styles.headerAlignementCenter, {width: 40, height: 40}]} />
    );
  };

  const _renderIconRight = () => {
    return (
      <View style={[styles.headerAlignementCenter, {width: 40, height: 40}]} />
    );
  };

  const _renderHeaderCenter = () => {
    return (
      <TouchableOpacity
        style={[
          styles.headerAlignementCenter,
          {
            width: getWindowWidth() - 130,
            paddingTop: 8,
          },
        ]}
        // onPress={() => (this.props.goBack ? this.props.goBack() : null)}
      >
        <Image
          source={res.images.logo}
          style={{
            width: 150,
            height: 20,
            resizeMode: 'contain',
            alignSelf: 'center',
          }}
        />
      </TouchableOpacity>
    );
  };

  return (
    <Header
      containerStyle={[
        styles.headerAlignementCenter,
        {
          backgroundColor: res.colors.ColorBlackO0,
          flexDirection: 'row',
          borderBottomWidth: 0,
        },
      ]}
      backgroundImage={res.images.back_ground_menu}
      backgroundImageStyle={{resizeMode: 'cover'}}
      barStyle="default"
      placement="center"
      leftComponent={_renderIconLeft()}
      centerComponent={_renderHeaderCenter()}
      rightComponent={_renderIconRight()}
    />
  );
}
