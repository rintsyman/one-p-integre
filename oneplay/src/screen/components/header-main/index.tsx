/*************Import from react lib********************/
import React, {useState} from 'react';
import {
  Text,
  View,
  Image,
  ImageBackground,
  Dimensions,
  StyleSheet,
  Alert,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';

import {connect} from 'react-redux';

import {ifIphoneX} from 'react-native-iphone-x-helper';

/*************Import from source code********************/

import res from '../../../resources/index';
import {styles} from './styles';
import {Header, Badge} from 'react-native-elements';
import {getWindowWidth} from '../header';
import ModalMenuDraw from '../../container/main-menu/menu-draw/index';
type IProps = {
  showNotif: () => void;
};
export const HeaderLogoCenter = (props: IProps) => {
  const [isShowModal, setisShowModal] = useState<boolean>(false);
  /*************Render PARTS********************/

  const _renderIconLeft = () => {
    return (
      <TouchableOpacity
        style={[styles.headerAlignementCenter, {width: 45, height: 45}]}
        onPress={() => props.showNotif()}>
        <Image
          source={res.images.notif}
          style={{
            width: 30,
            height: 30,
            resizeMode: 'contain',
            alignSelf: 'center',
          }}
        />

        <Badge
          //status="error"
          status={'error'}
          badgeStyle={{borderWidth: 0}}
          containerStyle={{
            position: 'absolute',
            top: 0,
            right: 0,
            padding: 0,
            borderWidth: 0,
          }}
          value={3}
        />
      </TouchableOpacity>
    );
  };

  const _renderIconRight = () => {
    return (
      <TouchableOpacity
        style={[styles.headerAlignementCenter, {width: 45, height: 45}]}
        onPress={() => setisShowModal(true)}>
        <Image
          source={res.images.burger}
          style={{
            width: 32,
            height: 32,
            resizeMode: 'contain',
            alignSelf: 'center',
          }}
        />
      </TouchableOpacity>
    );
  };

  const _renderHeaderCenter = () => {
    return (
      <TouchableOpacity
        style={[
          styles.headerAlignementCenter,
          {width: getWindowWidth() - 130, flexDirection: 'row', paddingTop: 8},
        ]}>
        <Image
          source={res.images.logo}
          style={{
            width: 150,
            height: 20,
            resizeMode: 'contain',
            alignSelf: 'center',
          }}
        />
      </TouchableOpacity>
    );
  };
  /*************Render methode********************/

  return (
    <>
      <Header
        containerStyle={[
          styles.headerAlignementCenter,
          {
            backgroundColor: res.colors.ColorBlackO0,
            flexDirection: 'row',

            borderBottomWidth: 0,
            ...ifIphoneX(
              {
                height: 100,
                paddingTop: 30,
              },
              {
                height: 80,
                paddingTop: 0,
              },
            ),
          },
        ]}
        backgroundImage={res.images.back_ground_menu}
        barStyle="default"
        placement="center"
        leftComponent={_renderIconLeft()}
        centerComponent={_renderHeaderCenter()}
        rightComponent={_renderIconRight()}
      />
      <ModalMenuDraw
        modalVisible={isShowModal}
        closeModal={() => setisShowModal(false)}
      />
    </>
  );
};
