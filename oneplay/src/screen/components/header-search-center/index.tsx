import React from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import res from '../../../resources/index';

import {Badge, Header} from 'react-native-elements';
import {getWindowWidth} from '../header';
import {HeaderStyle} from './style';
export default function Index() {
  const _renderIconLeft = () => {
    return (
      <TouchableOpacity
        style={[HeaderStyle.headerAlignementCenter, {width: 45, height: 45}]}
        onPress={() => null}>
        <Image
          source={res.images.notif}
          style={{
            width: 30,
            height: 30,
            resizeMode: 'contain',
            alignSelf: 'center',
          }}
        />

        <Badge
          status={'error'}
          badgeStyle={{borderWidth: 0}}
          containerStyle={{
            position: 'absolute',
            top: 0,
            right: 0,
            padding: 0,
            borderWidth: 0,
          }}
          //value={this.props.generalInfoReducer.nbrNotif}
          value={8}
        />
      </TouchableOpacity>
    );
  };

  const _renderIconRight = () => {
    return (
      <TouchableOpacity
        style={[HeaderStyle.headerAlignementCenter, {width: 45, height: 45}]}
        onPress={() => null}>
        <Image
          source={res.images.burger}
          style={{
            width: 32,
            height: 32,
            resizeMode: 'contain',
            alignSelf: 'center',
          }}
        />
      </TouchableOpacity>
    );
  };

  const _renderHeaderCenter = () => {
    return (
      <View
        style={[
          HeaderStyle.headerAlignementCenter,
          {
            width: getWindowWidth() - 130,
            flexDirection: 'row',
            justifyContent: 'flex-end',
          },
        ]}>
        <TouchableOpacity style={HeaderStyle.searchClick} onPress={() => null}>
          <Image
            source={res.images.search_icon}
            style={HeaderStyle.searchClickimage}
          />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <LinearGradient
      style={{
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: res.colors.ColorWhiteO25,
      }}
      colors={[
        'rgba(7, 12, 87, .73)',
        'rgba(3, 5, 43, .95)',
        'rgba(3, 5, 43, 1)',
      ]}
      start={{x: 0, y: 0}}
      end={{x: 0, y: 1}}
      locations={[0, 0.9, 1]}>
      <Header
        containerStyle={[
          HeaderStyle.headerAlignementCenter,
          {
            backgroundColor: res.colors.ColorBlackO0,
            flexDirection: 'row',

            borderBottomWidth: 0,
          },
        ]}
        barStyle="default"
        placement="center"
        leftComponent={_renderIconLeft()}
        centerComponent={_renderHeaderCenter()}
        rightComponent={_renderIconRight()}
      />
    </LinearGradient>
  );
}
