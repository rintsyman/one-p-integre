import {StyleSheet} from 'react-native';
import res from '../../../resources/index';
export const HeaderStyle = StyleSheet.create({
  headerAlignementCenter: {
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    position: 'relative',
    zIndex: 9999,
  },
  fleshBack: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
    // backgroundColor:'red'
  },
  logoLeft: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
  },
  searchClick: {
    borderRadius: 30,
    backgroundColor: '#03052C',
    alignContent: 'center',
    justifyContent: 'flex-start',
    paddingRight: 100,
    paddingBottom: 5,
    paddingTop: 5,
    paddingLeft: 5,
  },
  searchClickimage: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
  },
});
