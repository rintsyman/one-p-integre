/*************Import from react lib********************/
import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  ImageBackground,
  Dimensions,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {ifIphoneX} from 'react-native-iphone-x-helper';

/*************Import from source code********************/
import res from '../../../resources/index';
import {HeaderStyle} from './styles';
import {Header} from 'react-native-elements';
import {getWindowWidth} from '../../../utils/utils';

export const HeaderFond = () => {
  /*************Render Events********************/

  /*************Render PARTS********************/

  const _renderIconLeft = () => {
    return (
      <TouchableOpacity
        style={[HeaderStyle.headerAlignementCenter, {width: 40, height: 40}]}
        onPress={() => null}>
        <Image source={res.images.flesh_colors} style={HeaderStyle.fleshBack} />
      </TouchableOpacity>
    );
  };

  const _renderIconRight = () => {
    return (
      <View
        style={[HeaderStyle.headerAlignementCenter, {width: 40, height: 40}]}
      />
    );
  };

  const _renderHeaderCenter = () => {
    return (
      <TouchableOpacity
        style={[
          HeaderStyle.headerAlignementCenter,
          {width: getWindowWidth() - 130},
        ]}
        onPress={() => null}>
        <Image
          source={res.images.logo}
          style={{
            width: 150,
            height: 20,
            resizeMode: 'contain',
            alignSelf: 'center',
          }}
        />
      </TouchableOpacity>
    );
  };

  /*************Render methode********************/

  return (
    <Header
      containerStyle={[
        HeaderStyle.headerAlignementCenter,
        {
          backgroundColor: res.colors.ColorBlackO0,
          flexDirection: 'row',
          borderBottomWidth: 0,
          ...ifIphoneX(
            {
              height: 100,
              paddingTop: 30,
            },
            {
              height: 80,
              paddingTop: 0,
            },
          ),
        },
      ]}
      backgroundImage={res.images.back_ground_menu}
      backgroundImageStyle={{resizeMode: 'cover'}}
      barStyle="default"
      placement="center"
      leftComponent={_renderIconLeft()}
      centerComponent={_renderHeaderCenter()}
      rightComponent={_renderIconRight()}
    />
  );
};
