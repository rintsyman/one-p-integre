import {Platform, Dimensions} from 'react-native';
export const isIOS = () => Platform.OS === 'ios';

export const isAndroid = () => Platform.OS === 'android';

export const isDevEnv = () => __DEV__ === true;

export const getDeviceVersion = () => Platform.Version;

export const getWindowHeight = () => Dimensions.get('window').height;

export const getWindowWidth = () => Dimensions.get('window').width;

export const getPageLimit = () => 10;

export const getDateNow = () => {
  var date = new Date().getDate(); //Current Date
  var month = new Date().getMonth() + 1; //Current Month
  var year = new Date().getFullYear(); //Current Year
  var fromDate = `${year}-${month}-${date}`;
  return fromDate;
};
