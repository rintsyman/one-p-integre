/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {StatusBar} from 'react-native';
import SplashScreen from './src/screen/container/splashScreen';
import Signin from './src/screen/container/flow-auth/signin/index';
import Signup from './src/screen/container/flow-auth/signup/index';
import CompleteSignup from './src/screen/container/flow-auth/complete-signup/index';
import EnterMatch from './src/screen/container/main-menu/menu-one/enter-match/index';
import Match from './src/screen/container/main-menu/menu-one/in-out-quiz/index';
import Flash from './src/screen/container/main-menu/menu-one/flash/index';
import Post from './src/screen/container/main-menu/menu-one/flash/view-post/index';
import Home from './src/screen/container/main-menu/menu-one/accueil/index';
import Notification from './src/screen/container/main-menu/notification/index';
import {AllNotification} from './src/screen/container/main-menu/notification/view-all-notif/index';
import Camera from './src/screen/container/main-menu/menu-two/camera';
import {NavigationContainer} from '@react-navigation/native';
import LoginFlow from './src/routes/authentification-stack/index';
import Index from './src/routes/app-tab-navigation';
import Custom from './src/routes/stack-tab-custom';
import {AppContext} from './src/provider/index';

const App = () => {
  const [simulateSignin, setSimulateSignin] = useState<boolean>(false);

  return (
    <>
      <StatusBar hidden={true} />
      <NavigationContainer>
        <AppContext.Provider
          value={{
            simulateSignin: simulateSignin,
            setSignin: (p: boolean) => setSimulateSignin(p),
          }}>
          {simulateSignin ? <Custom /> : <LoginFlow />}
        </AppContext.Provider>
      </NavigationContainer>
    </>
  );
};

export default App;
